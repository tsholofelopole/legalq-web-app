import { LoginComponent } from './view-components/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ForgotPasswordComponent } from './view-components/forgot-password/forgot-password.component';


export const AppRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
    {
      path: 'forgot-password',
      component: ForgotPasswordComponent
    }

];
export const routing: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
