import { Component, OnInit } from '@angular/core';
// import { Idle } from 'idlejs/dist';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { CustomLoaderService } from './services/view-services/cuctom-loader.service';
import { SessionManagerService } from './services/view-services/session-manager.service';
import { DisplayQuoteManager } from './services/view-services/display-quote-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  displayLoader = false;
  sessionExpired = false;
  displayQuote = false;

  title = 'legalq-web-app';

  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;


  constructor(private idle: Idle, private keepalive: Keepalive,
              private customLoaderService: CustomLoaderService,
              private sessionManagerService: SessionManagerService,
              private displayQuoteManager: DisplayQuoteManager) {

    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(900);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(10);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
    });
    idle.onIdleStart.subscribe(() => {

      this.sessionManagerService.displaySessionManagerMessage(true);
      console.log('\n\nIDLE\n\n');
      this.idleState = 'You\'ve gone idle!';
    });

    idle.onTimeoutWarning.subscribe((countdown) => {
      console.log('\n\nIDLE warning\n\n');
      this.idleState = 'You will time out in ' + countdown + ' seconds!';
    }
   );
    // sets the ping interval to 15 seconds
    keepalive.interval(15);
    keepalive.onPing.subscribe(() => this.lastPing = new Date());
    this.reset();
  }

  ngOnInit(): void {

    this.sessionManagerService.currentState.subscribe(state => this.sessionExpired = state);
    this.customLoaderService.currentState.subscribe(state => this.displayLoader = state);
    this.displayQuoteManager.currentdisplayQuote.subscribe(state => this.displayQuote = state);
    //throw new Error('Method not implemented.');
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  // constructor() {
  //   const idle = new Idle()
  //   .whenNotInteractive()
  //   .within(600)
  //   .do(() => console.log('\n\nIDLE'))
  //   .start();
  // }


}
