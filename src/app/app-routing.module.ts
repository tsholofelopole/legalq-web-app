import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './view-components/login/login.component';
import { ForgotPasswordComponent } from './view-components/forgot-password/forgot-password.component';
import { HomeComponent } from './view-components/home/home.component';
import { AdminComponent } from './view-components/admin/admin.component';
import { HistoryComponent } from './view-components/history/history.component';
import { QuoteComponent } from './view-components/quote/quote.component';
import { RequestpasswordchangeComponent } from './view-components/requestpasswordchange/requestpasswordchange.component';
import { MattertypeComponent } from './view-components/mattertype/mattertype.component';
import { QuotetypeComponent } from './view-components/quotetype/quotetype.component';
import { QuoteSummaryComponent } from './view-components/quote/quote-summary/quote-summary.component';

const routes: Routes = [
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
    {
      path: 'forgot-password',
      component: ForgotPasswordComponent
    },
    {
      path: 'home',
      component: HomeComponent
    },
    {
      path: 'admin',
      component: AdminComponent
    },
    {
      path: 'history',
      component: HistoryComponent
    },
    {
      path: 'quote',
      component: QuoteComponent
    },
    {
      path: 'reset-password',
      component: RequestpasswordchangeComponent
    },
    {
      path: 'mattertype',
      component: MattertypeComponent
    },
    {
      path: 'quotetype',
      component: QuotetypeComponent
    },
    {
      path: 'quotesummary',
      component: QuoteSummaryComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  {enableTracing: true, useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
