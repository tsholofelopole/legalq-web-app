import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  mobile = false;
  mobileDevice = false;
  screenwidth: any;

  constructor(private router: Router,
              private customLoaderService: CustomLoaderService) { }
  onResize(event) {
    if (event.target.innerWidth < 997) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }

  ngOnInit() {

    if (window.innerWidth < 997) {
      this.mobile = true;
    }

    this.screenwidth = window.innerWidth;
    // if (this.screenwidth < 768 ) {
    //   this.mobile = true;
    // }
  }

  goHome() {
    // go to home page once that is done
    this.router.navigate(['/home']);
  }

  logout() {
    // probably clear session storage first
    this.router.navigate(['/login']);
  }

  createQuotePage() {
    this.router.navigate(['/mattertype']);
  }

  goToHistory() {
    // this.customLoaderService.updateLoaderState(true);
    this.router.navigate(['/history']);
  }

  goToAdminPage() {
    // this.customLoaderService.updateLoaderState(true);
    this.router.navigate(['/admin']);
  }

}
