import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { BsDropdownModule, } from 'ngx-bootstrap/dropdown';
import { RouterModule } from '@angular/router';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),

    RouterModule
  ],
  exports: [HeaderComponent],
  providers: [
    CustomLoaderService
  ]
})
export class HeaderModule { }
