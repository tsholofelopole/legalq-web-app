import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionManagerService } from 'src/app/services/view-services/session-manager.service';

@Component({
  selector: 'app-sessionmanager',
  templateUrl: './sessionmanager.component.html',
  styleUrls: ['./sessionmanager.component.css']
})
export class SessionmanagerComponent implements OnInit {

  clicked = false;
  constructor(private router: Router,
              private sessionManagerService: SessionManagerService) { }

  ngOnInit() {
  }

  continueSession() {
    this.sessionManagerService.displaySessionManagerMessage(false);
  }

  endSession() {
    // end session
    // clear session storage
    this.router.navigate(['/login']);
    this.sessionManagerService.displaySessionManagerMessage(false);
  }

}
