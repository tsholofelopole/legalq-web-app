import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionmanagerComponent } from './sessionmanager.component';
import { HeaderModule } from '../header/header.module';
import { BrowserModule } from '@angular/platform-browser';
import { SessionManagerService } from 'src/app/services/view-services/session-manager.service';

@NgModule({
  declarations: [SessionmanagerComponent],
  imports: [
    CommonModule,
    BrowserModule,
  ],
  exports: [SessionmanagerComponent],
  providers: [SessionManagerService]
})
export class SessionmanagerModule { }
