import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionmanagerComponent } from './sessionmanager.component';

describe('SessionmanagerComponent', () => {
  let component: SessionmanagerComponent;
  let fixture: ComponentFixture<SessionmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
