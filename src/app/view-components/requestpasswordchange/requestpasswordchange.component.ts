import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-requestpasswordchange',
  templateUrl: './requestpasswordchange.component.html',
  styleUrls: ['./requestpasswordchange.component.css']
})
export class RequestpasswordchangeComponent implements OnInit {

  resetpasswordForm: FormGroup;
  clicked = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router) { }


  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.resetpasswordForm = this.formBuilder.group({
      email: ['', [
        Validators.required
      ]],
    });
  }

  resetPassword() {
    // send link to reset password to email
    // call service to reset password
  }

  goToHomeScreen() {
    this.router.navigate(['/login']);
  }

}
