import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestpasswordchangeComponent } from './requestpasswordchange.component';

describe('RequestpasswordchangeComponent', () => {
  let component: RequestpasswordchangeComponent;
  let fixture: ComponentFixture<RequestpasswordchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestpasswordchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestpasswordchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
