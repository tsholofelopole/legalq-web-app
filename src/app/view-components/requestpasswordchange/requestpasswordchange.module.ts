import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestpasswordchangeComponent } from './requestpasswordchange.component';
import { HeaderModule } from '../header/header.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [RequestpasswordchangeComponent],
  imports: [
    CommonModule,
    HeaderModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [RequestpasswordchangeComponent]
})
export class RequestpasswordchangeModule { }
