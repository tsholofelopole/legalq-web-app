import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { HeaderModule } from '../header/header.module';
import { FormBuilder, FormGroup, NgForm, Validators, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ForgotPasswordModule } from '../forgot-password/forgot-password.module';
import { LegalModule } from 'src/app/services/legal/legal.module';
import { AuthenticatorModule } from 'src/app/services/legal/authenticator/authenticator.module';
import { HelperServicesModule } from 'src/app/services/helper-services/helper-services.module';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    HeaderModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LegalModule,
    AuthenticatorModule,
    HelperServicesModule

  ],
  exports: [LoginComponent],
  providers: [
    CustomLoaderService,
  ]
})
export class LoginModule { }
