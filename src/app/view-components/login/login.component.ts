import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticateUserManager } from 'src/app/services/legal/authenticator/authenicate-user/authenticate-user-manager.service';
import { AuthenticateUserResponse } from 'src/app/services/legal/authenticator/models/authenticate-user-reponse.model';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  clicked = false;
  formUserNameInput = true;
  formPasswordInput = true;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authenticateUserManager: AuthenticateUserManager,
              private firmInSessionService: FirmInSessionService,
              private customLoaderService: CustomLoaderService
              ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', [
        Validators.required
      ]],
      password: ['', [
        Validators.required
      ]]
    });
  }

  login() {
    this.clicked = true;

    if (this.loginForm.valid) {
      // this.headerService.changeUserName(this.loginForm.get("username").value.trim());

      this.customLoaderService.updateLoaderState(true);
      const username = this.loginForm.get('username').value.trim();
      const password = this.loginForm.get('password').value.trim();
      this.authenticateUserManager.authenticateUser(username, password);

      // console.log("Username: ", username, " Password: ", password);
      this.authenticateUserManager.authenticateUserResponseObservable.subscribe(
        response => {
          console.log('\n\nLogged in user response: ', response);
          if (response instanceof AuthenticateUserResponse) {
            if (response.authenticationAdvice.authenticated) {

              this.firmInSessionService.setFirmLegalReference(response.authenticationAdvice.systemUser.firmLegalReference);
              // this.firmInSessionService.setFirmLegalReference(response.authenticationAdvice.firm.legalReference);
              this.customLoaderService.updateLoaderState(false);
              this.router.navigate(['/home']);

            } else {
              // display error message the reenable button
              this.clicked = false;
              // precondition violation of some setRootDomAdapter, handle
            }
          } else {
            this.clicked = false;
            // Unexpected response type, handle
          }

      });

    } else {
      const errormessage = 'Failed to log in. Please enter your username and password.';
    }

  }

  applyInputError() {
    if (!this.loginForm.controls.username.valid) {
      this.formUserNameInput = false;
    }
    if (!this.loginForm.controls.password.valid) {
      this.formPasswordInput = false;
    }
  }

  forgotPassword() {
    this.router.navigate(['/reset-password']);
  }



}
