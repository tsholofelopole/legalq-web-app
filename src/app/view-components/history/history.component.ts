import { Component, OnInit, Input } from '@angular/core';
// tslint:disable-next-line: max-line-length
import { QuoteHistoryPersistenceManagement } from 'src/app/services/legal/quote-history-persistence/quote-history-persistence-management.service';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { UnexpectedServiceError } from 'src/app/services/helper-services/exception-handling/unexpected-service-error.model';
import { RequestNotValidException } from 'src/app/services/helper-services/exception-handling/request-not-valid-exception.model';
import { FindQuoteHistoryRequest } from 'src/app/services/legal/quote-history-persistence/models/find-quote-history-request.model';
import { QuoteHistoryCriteria } from 'src/app/models/quote-history-criteria.model';
import { QuoteHistory } from 'src/app/models/quote-history.model';
import { FindQuoteHistoryResponse } from 'src/app/services/legal/quote-history-persistence/models/find-quote-history-response.model';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  firm = '';
  mobile = false;

  historyItems = [
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
    {refNumber: '23232323232323232', date: '2019-12-03T10:30', client: 'Absa'},
  ];

  quoteHistories: QuoteHistory[] = [];
  noQuoteHistoriesToDisplay = false;

  constructor(private firmInSessionManager: FirmInSessionService,
              private quoteHistoryManager: QuoteHistoryPersistenceManagement,
              private customLoaderService: CustomLoaderService) { }

  onResize(event) {
    if (event.target.innerWidth < 997) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }

  ngOnInit() {

    if (window.innerWidth < 997) {
      this.mobile = true;
    }

    this.firmInSessionManager.currentFirmLegalReference.subscribe(firm => {
      this.firm = firm;
      const findQuoteHistoryRequest = new FindQuoteHistoryRequest();
      const quoteHistoryCriteria = new QuoteHistoryCriteria();
      quoteHistoryCriteria.firmLegalReference = this.firm;
      findQuoteHistoryRequest.criteria = quoteHistoryCriteria;
      this.quoteHistoryManager.findQuoteHistory(findQuoteHistoryRequest);

      this.quoteHistoryManager.findQuoteHistoryResponseObservable.subscribe(response => {
        if (response instanceof FindQuoteHistoryResponse) {
          this.quoteHistories = response.quoteHistories;
          if (this.quoteHistories.length < 1) {
            this.noQuoteHistoriesToDisplay = true;
          }
          this.customLoaderService.updateLoaderState(false);
        }

      });

    });

    // check exceptions
    this.quoteHistoryManager.exception.subscribe(exception => {
      if (exception instanceof UnexpectedServiceError) {
        // do something
      } else if (exception instanceof RequestNotValidException) {
        // do something
      }
    });
  }

  previewQuoteHistory(historyRef) {
    // call service to get quote details
    // alert('ref: ' + historyRef);
  }

}

