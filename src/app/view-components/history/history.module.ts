import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryComponent } from './history.component';
import { HeaderModule } from '../header/header.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HistorycontentModule } from '../historycontent/historycontent.module';
import { LegalModule } from 'src/app/services/legal/legal.module';
import { HelperServicesModule } from 'src/app/services/helper-services/helper-services.module';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

@NgModule({
  declarations: [HistoryComponent],
  imports: [
    CommonModule,
    HeaderModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HistorycontentModule,
    LegalModule,
    HelperServicesModule
  ],
  exports: [HistoryComponent],
  providers: [CustomLoaderService]
})
export class HistoryModule { }
