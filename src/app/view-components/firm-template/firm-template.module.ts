import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirmTemplateDocumentComponent } from './firm-template.component';
import { DocumentTemplateManagerService } from 'src/app/services/view-services/document-template-manager.service';
import { DisplayQuoteManager } from 'src/app/services/view-services/display-quote-manager.service';

@NgModule({
  declarations: [FirmTemplateDocumentComponent],
  imports: [
    CommonModule
  ],
  exports: [FirmTemplateDocumentComponent],
  providers: [ DocumentTemplateManagerService,
    DisplayQuoteManager]
})
export class FirmTemplateModule { }
