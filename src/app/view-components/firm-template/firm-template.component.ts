import { Component, OnInit } from '@angular/core';
import { DocumentTemplateManagerService } from 'src/app/services/view-services/document-template-manager.service';
import { DisplayQuoteManager } from 'src/app/services/view-services/display-quote-manager.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-firm-template-doc',
  templateUrl: './firm-template.component.html',
  styleUrls: ['./firm-template.component.css']
})
export class FirmTemplateDocumentComponent implements OnInit {

  constructor(private documentTemplateManagerService: DocumentTemplateManagerService,
              private displayQuoteManager: DisplayQuoteManager) { }

  firmImageHeader: string; // get images header as a byte string
  firmImageFooter: string; // get image footer as string

  base64textString = '';
  headerImage = '';
  pdfUrl = '';
  footerImage = '';
 
  ngOnInit() {

    const doc = new jsPDF('p', 'pt');
    // doc.setFontSize(12);
    doc.setTextColor(0);
    doc.setFontStyle('normal');

    const col = [['No.'], ['Name'], ['Description'], ['Amount'], ['Disbursement']];
    const rows = [];

    const head = [
      [
        { content: 'Quote', colSpan: 5, styles: { halign: 'center', fillColor: [22, 160, 133] } },
      ],
      ['ITEM', 'DESCRIPTION', 'FEE', 'DISBURSEMENT']
    ];


    this.documentTemplateManagerService.currentDocHeaderValue.subscribe(response => {
      this.headerImage = response;
      this.documentTemplateManagerService.currentfooterImageUri.subscribe(footer => this.firmImageFooter = footer);
      const headerImageData = 'data:image/png;charset=UTF-8;base64,' + this.headerImage;
      const footerImageData = 'data:image/png;charset=UTF-8;base64,' + this.firmImageFooter;

      // addImage(imageData, format, x, y, width, height, alias, compression, rotation)
      doc.addImage(headerImageData, 'png', 0, 0, 592, 80);
      doc.addImage(footerImageData, 'png', 0, 770, 592, 90);
      doc.output('pdfjsnewwindow', { filename: 'firm_template.pdf' });
      window.open(doc.save('firm_template.pdf'), '_blank');
    });

    setTimeout(() => {
      this.displayQuoteManager.displayQuoteStateUpdate(false);
    }, 2000);
  }

  footer(doc, footerImageData) {
    const pageSize = doc.internal.pageSize;
    const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
    const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    doc.addImage(footerImageData, 'png', 0, 600, 592, 90);
    const str = 'Page ' + doc.internal.getNumberOfPages();
    doc.setFontSize(10);
  }


}
