import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherLoaderComponent } from './other-loader.component';

describe('OtherLoaderComponent', () => {
  let component: OtherLoaderComponent;
  let fixture: ComponentFixture<OtherLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
