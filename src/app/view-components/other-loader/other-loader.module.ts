import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OtherLoaderComponent } from './other-loader.component';

@NgModule({
  declarations: [OtherLoaderComponent],
  imports: [
    CommonModule
  ],
  exports: [OtherLoaderComponent]
})
export class OtherLoaderModule { }
