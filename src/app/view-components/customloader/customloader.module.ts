import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomloaderComponent } from './customloader.component';

@NgModule({
  declarations: [CustomloaderComponent],
  imports: [
    CommonModule
  ],
  exports: [CustomloaderComponent],
})
export class CustomloaderModule { }
