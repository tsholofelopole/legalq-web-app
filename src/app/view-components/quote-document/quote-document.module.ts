import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuoteDocumentComponent } from './quote-document.component';
import { DocumentTemplateManagerService } from 'src/app/services/view-services/document-template-manager.service';
import { DisplayQuoteManager } from 'src/app/services/view-services/display-quote-manager.service';

@NgModule({
  declarations: [QuoteDocumentComponent],
  imports: [
    CommonModule
  ],
  exports: [QuoteDocumentComponent],
  providers: [ DocumentTemplateManagerService,
    DisplayQuoteManager]
})
export class QuoteDocumentModule { }
