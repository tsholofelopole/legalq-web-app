import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteDocumentComponent } from './quote-document.component';

describe('QuoteDocumentComponent', () => {
  let component: QuoteDocumentComponent;
  let fixture: ComponentFixture<QuoteDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
