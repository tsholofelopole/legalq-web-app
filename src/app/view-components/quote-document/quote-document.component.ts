import { Component, OnInit } from '@angular/core';
import { StandardCostItem } from 'src/app/models/standard-cost-item.model';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { DocumentTemplateManagerService } from 'src/app/services/view-services/document-template-manager.service';
import { DisplayQuoteManager } from 'src/app/services/view-services/display-quote-manager.service';
// import * as jsPDF from '../../../assets/js/jspdf.min.js';
// import 'jspdf-autotable';
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');
// declare var jsPDF: any;

@Component({
  selector: 'app-quote-document',
  templateUrl: './quote-document.component.html',
  styleUrls: ['./quote-document.component.css']
})
export class QuoteDocumentComponent implements OnInit {

  constructor(private documentTemplateManagerService: DocumentTemplateManagerService,
              private displayQuoteManager: DisplayQuoteManager) { }

  firmImageHeader: string; // get images header as a byte string
  firmImageFooter: string; // get image footer as string
  // scis: StandardCostItem[] = [];
  sci =     {
    '@class': 'org.legalq.legal.StandardCostItem',
    matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
    name: 'Consulation',
    description: 'Test SCI Object',
    seqNumber: 1,
    rate: 2000,
    unitOfMeasurement: {
      '@class': 'org.legalq.legal.UnitOfMeasurement',
      name: 'Hourly',
      description: 'Measurement value for Consultation hourly rate',
      code: 'B300',
      active: 2,
    },
    canHaveDisbursement: true,
    canEmployCounsel: true,
    active: 1,
    matterTypeEnum: null,
    disbFlag: false,
    matterPhaseInt: 1,
    quantity: 1
  };
  scis = [
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 1,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'File and Service',
      description: 'Test SCI Object',
      seqNumber: 2,
      rate: 200,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation with counsel',
      description: 'Test SCI Object',
      seqNumber: 4,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Pages',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: false,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Peruse particulars of claim',
      description: 'Test SCI Object',
      seqNumber: 5,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: false,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    }
  ];

  base64textString = '';
  headerImage = '';
  pdfUrl = '';
  footerImage = '';
  clientType = 'PLANTIFF';
  sideOne = "Test Client";
  sideTwo = "Test Other Client";

  // placeholder for total number of pages
  totalPagesExp = '{total_pages_count_string}';

  ngOnInit() {

    const doc = new jsPDF('p', 'pt');
    // doc.setFontSize(12);
    doc.setTextColor(0);
    doc.setFontStyle('normal');

    const col = [['No.'], ['Name'], ['Description'], ['Amount'], ['Disbursement']];
    const rows = [];

    let head = [
    [
        {content: 'Quote', colSpan: 5, styles: {halign: 'center', fillColor: [22, 160, 133]}},
    ],
        ['ITEM', 'DESCRIPTION', 'FEE', 'DISBURSEMENT']
    ];
// tslint:disable-next-line: forin
    for (let sci in this.scis) {
      let temp = [[this.sci.seqNumber], [this.sci.description],
        [this.sci.rate],  [this.sci.quantity]];
        rows.push(temp);
    }

    this.documentTemplateManagerService.currentDocHeaderValue.subscribe(response => {
        this.headerImage = response;
        this.documentTemplateManagerService.currentfooterImageUri.subscribe(footer => this.firmImageFooter = footer);
        const headerImageData = 'data:image/png;charset=UTF-8;base64,' + this.headerImage;
        const footerImageData = 'data:image/png;charset=UTF-8;base64,' + this.firmImageFooter;

        // addImage(imageData, format, x, y, width, height, alias, compression, rotation)
        doc.addImage(headerImageData, 'png', 0, 0, 592, 80);
        // text(text, x, y, optionsopt, transform)
        // doc.text('Standard Cost Items', 296, 105, {align: 'center', lineHeightFactor: '2', margin: '15px'});
        doc.text('COST ESTIMATE FOR THE MATTER BETWEEN:', 296, 100, {align: 'center', lineHeightFactor: '2',});
        doc.text(this.sideOne + ' / ' + this.sideTwo, 296, 120, {align: 'center', lineHeightFactor: '2',});
        doc.text('AS BETWEEN ATTORNEY AND OWN CLIENT', 296, 135, {align: 'center', lineHeightFactor: '2',});
        doc.text('ACTING ON BEHALF OF THE' + this.clientType, 296, 150, [{align: 'center', lineHeightFactor: '2',}]);

        const totalPagesExp = '{total_pages_count_string}'; // placeholder for total number of pages
        doc.autoTable({startY: 200, head: head, body: rows, theme: 'grid',
          // let fileName = 'Statement.pdf',
          bodyStyles: {
            margin: 40,
            fontSize: 10,
            lineWidth: 0.2,
            lineColor: [0, 0, 0] },
        });
        doc.addImage(footerImageData, 'png', 0, 770, 592, 90);
        doc.text('Page ' + doc.internal.getNumberOfPages(), 296, 830);
        doc.output('pdfjsnewwindow', {filename: 'quote.pdf'});
        doc.text('LegalQ 2019', -5, 832, {align: 'left', fontSize: '9pt',  });

        //doc.save('quote.pdf');
        //window.open(doc.output('application/pdf', {filename: 'quote.pdf'}), '_blank');
        window.open(doc.save('quote.pdf'), '_blank');

        setTimeout(() => {
          this.displayQuoteManager.displayQuoteStateUpdate(false);
        }, 2000);
        //this.pdfUrl = './quote.pdf'; // doc.save('quote.pdf');
    });

  }

  footer(doc, footerImageData) {
    let pageSize = doc.internal.pageSize;
    let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
    let pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    doc.addImage(footerImageData, 'png', 0, 600, 592, 90);
    // doc.autoTable({

    //   html: '#footer_text_pdf',
    //   startY: pageHeight - 50,
    //   styles: {
    //       halign: 'center',
    //       cellPadding: 0.2
    //   }
    // });
    let str = 'Page ' + doc.internal.getNumberOfPages();
    // if (typeof doc.putTotalPages === 'function') {
    //   str = str + " of " + totalPagesExp;
    // }
    doc.setFontSize(10);
    //doc.text(str, data.settings.margin.left, pageHeight - 10);
      // if (typeof doc.putTotalPages === 'function') {
      //   doc.putTotalPages(totalPagesExp);
      // }
  };



}



function creatPdf(sci) {
  let doc = new jsPDF();

  doc.autoTable({
    styles: {fillColor: [255, 0, 0]},
    columnStyles: {0: {haligan: 'center', fillColor: [0, 255, 0]}},
    margin: {top: 50},
    head: [['No.', 'Name', 'Description', 'Amount', 'Disbursement']],
    body: [{No: sci.seqNumber, Name: sci.name, Description: sci.description,
          Amount: sci.rate, Disbursement: sci.quantity }],
    columns: [],
  });

  this.doc.save('table.pdf');
}
function generatePDF(sci) {
  // generate pdf using header, footer images and scis
  // scis will be displayed in a table using npm autotable

  this.doc.autoTable({
    styles: {
        cellPadding: 0.5,
        fontSize: 12
    },
    // startY: 30, /* if start position is fixed from top */
    tableLineColor: [0, 0, 0], // choose RGB
    tableLineWidth: 0.5, // table border width
    head: this.doc.headRows(), // define head rows
    body: this.doc.bodyRows(this.doc.pdfBody.length, this.doc.pdfBody),
    /*first param is the number of rows in total and second
     param is the actual content. This is the actual body of the pdf between the header and footer*/
    bodyStyles: {
        margin: 40,
        fontSize: 10,
        lineWidth: 0.2,
        lineColor: [0, 0, 0]
    },

    /*whatever you write in didDrawPage comes in every page. Header or footer is determined from startY position in the functions.*/
    didDrawPage(data) {

        // Header
        this.doc.setFontSize(12);
        const fileTitle = 'Test document';
        // convert image tp base6 and add here, create external funtion to do the convertion

        const img = 'data:image/png;base64'; // use addImage as explained earlier.
        this.doc.text(fileTitle, 14, 35);
        this.doc.addImage(img, 'JPEG', 157, 10, 40, 20);

        // Footer
        const pageSize = this.doc.internal.pageSize;
        // jsPDF 1.4+ uses getHeight, <1.4 uses .height
        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
        // jsPDF 1.4+ uses getWidth, <1.4 uses .width
        const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
        this.doc.autoTable({
            html: '#footer_text_pdf',
            startY: pageHeight - 50,
            styles: {
                halign: 'center',
                cellPadding: 0.2
            }
        });
        let str = 'Page ' + this.doc.internal.getNumberOfPages();
        // Total page number plugin only available in jspdf v1.0+
        if (typeof this.doc.putTotalPages === 'function') {
            str = str + ' of ' + this.totalPagesExp;
        }
        this.doc.setFontSize(10);
        this.doc.text(str, data.settings.margin.left, pageHeight - 10);
    },
    margin: {
        bottom: 60, // this decides how big your footer area will be
        top: 40 // this decides how big your header area will be.
    }
});
// Total page number plugin only available in jspdf v1.0+
  if (typeof this.doc.putTotalPages === 'function') {
    this.doc.putTotalPages(this.totalPagesExp);
  }
  var filename = 'Statement.pdf';
  this.doc.save(filename); // this downloads a copy of the pdf in your local instance.
  }



