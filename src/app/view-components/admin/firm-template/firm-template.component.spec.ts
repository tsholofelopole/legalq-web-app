import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmTemplateComponent } from './firm-template.component';

describe('FirmTemplateComponent', () => {
  let component: FirmTemplateComponent;
  let fixture: ComponentFixture<FirmTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirmTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
