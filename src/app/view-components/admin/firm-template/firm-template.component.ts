import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Firm } from 'src/app/models/firm.model';
import { FirmPersistenceManagement } from 'src/app/services/legal/firm-persistence/firm-persistence-management.service';
import { RequestNotValidException } from 'src/app/services/helper-services/exception-handling/request-not-valid-exception.model';
import { UnexpectedServiceError } from 'src/app/services/helper-services/exception-handling/unexpected-service-error.model';
import { DocumentTemplateManagerService } from 'src/app/services/view-services/document-template-manager.service';
import { DisplayQuoteManager } from 'src/app/services/view-services/display-quote-manager.service';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { UpdateFirmResponse } from 'src/app/services/legal/firm-persistence/models/update-firm-response.model';
import { FindFirmResponse } from 'src/app/services/legal/firm-persistence/models/find-firm-response.model';

@Component({
  selector: 'app-firm-template',
  templateUrl: './firm-template.component.html',
  styleUrls: ['./firm-template.component.css']
})
export class FirmTemplateComponent implements OnInit, AfterViewInit {

  @Input() firmRef;
  firm: Firm;
  clicked = false;
  previewClicked = false;
  base64textString = '';
  headerBase64String = '';
  footerbase64String = '';
  updateMsg = '';

  constructor(private firmPersistenceManagement: FirmPersistenceManagement,
              private documentTemplateManagerService: DocumentTemplateManagerService,
              private displayQuoteManager: DisplayQuoteManager,
              private firmInSessionService: FirmInSessionService) { }

  ngOnInit() {
    this.firmInSessionService.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);
    this.firmPersistenceManagement.findFirm(this.firmRef);

    setTimeout(() => {
        this.firmPersistenceManagement.findFirmObservable.subscribe(response => {
          if (response instanceof FindFirmResponse) {
            this.firm = response.firm;
            // this.firmLoaded = true;
          }
        });
      // this.customLoaderService.updateLoaderState(false);
    }, 2000);

  }

  ngAfterViewInit() {

    // this.firmPersistenceManagement.findFirmObservable.subscribe(
    //   firmResponse => {
    //     console.log('\n\n******* Returned firm response: ', firmResponse);
    //     this.firm = firmResponse.firm;
    //     // this.firm = new Firm();
    //   }
    // );

    this.firmPersistenceManagement.findFirmException.subscribe(exception => {
      if (exception instanceof RequestNotValidException) {
        // handle errors
      }
      if (exception instanceof UnexpectedServiceError) {
        // handle errors
      } else {
        // default error catch
      }

    });
  }

  submitHeaderTemplate(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
        const reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  submitFooterTemplate(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
        const reader = new FileReader();
        reader.onload = this._handleFooterReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  _handleFooterReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.footerbase64String = btoa(binaryString);

    this.documentTemplateManagerService.updateFooterImage(this.footerbase64String);

    console.log(btoa(binaryString));
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.headerBase64String = btoa(binaryString);

    this.documentTemplateManagerService.setDocumentHeaderTemplateValue(this.headerBase64String);

    console.log(btoa(binaryString));
  }

  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
        const reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  updatefirmDetails() {
    // to be implemented after update method exists on the service
    this.documentTemplateManagerService.currentDocHeaderValue.subscribe(header => this.firm.headerImage = header);
    this.documentTemplateManagerService.currentfooterImageUri.subscribe(footer => this.firm.footerImage = footer);
    this.firmPersistenceManagement.updateFirm(this.firm);

    this.firmPersistenceManagement.updateFirmObservable.subscribe(updateResponse => {
      if (updateResponse instanceof UpdateFirmResponse) {
          this.updateMsg = 'Firm Header and footer images saved succesfully.';

          setTimeout(() => {
            this.updateMsg = '';
          }, 5000);
      }
    });
  }

  previewFirmTemplate() {
    this.displayQuoteManager.displayQuoteStateUpdate(true);
  }

}
