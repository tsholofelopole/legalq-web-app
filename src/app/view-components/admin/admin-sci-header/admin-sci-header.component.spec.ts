import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSciHeaderComponent } from './admin-sci-header.component';

describe('AdminSciHeaderComponent', () => {
  let component: AdminSciHeaderComponent;
  let fixture: ComponentFixture<AdminSciHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSciHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSciHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
