import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSciContentComponent } from './admin-sci-content.component';

describe('AdminSciContentComponent', () => {
  let component: AdminSciContentComponent;
  let fixture: ComponentFixture<AdminSciContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSciContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSciContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
