import { Component, OnInit, Input, SimpleChanges, ɵConsole, OnChanges, NgZone, SimpleChange, DoCheck } from '@angular/core';
import { UpperCasePipe } from '@angular/common';
// tslint:disable-next-line: import-spacing
import { StandardCostItemPersistenceManagement } from
'src/app/services/legal/standard-cost-item-persistence/standard-cost-item-persistence-management.service';
import { StandardCostItem } from 'src/app/models/standard-cost-item.model';
import { UnitOfMeasurement } from 'src/app/models/unit-of-measurement.model';
// tslint:disable-next-line: import-spacing
import { FirmRatePersistenceManagementService } from
'src/app/services/legal/firm-rate-persistence/firm-rate-persistence-management.service';
import { FindFirmRateRequest } from 'src/app/services/legal/firm-rate-persistence/models/find-firm-rate-request.model';
import { MatterType } from './../../../models/matter-type.model';


@Component({
  selector: 'app-admin-sci-content',
  templateUrl: './admin-sci-content.component.html',
  styleUrls: ['./admin-sci-content.component.css']
})

export class AdminSciContentComponent implements OnInit, OnChanges, DoCheck {

  @Input() scis;
  @Input() trialPhases;
  @Input() rates;
  selectedName = '';
  viewScis: StandardCostItem[] = [];

  // filters
  @Input()  clientAction !: string;
  @Input() mattertype;

  // filters
  motionAction = 'ACTION';
  matterType = 'RAF';

  matterActionMotion = [
    {name: 'ACTION'},
    {name: 'MOTION'}
  ];

  matterTypes = [
    {name: 'RAF'},
    {name: 'SUMMONS'},
    {name: 'DIVORCE'},
    {name: 'LIQUIDATION'},
    {name: 'INTERLOCULARY'},
    {name: 'OTHER'}
  ];

  uoms = [
    {name: 'Hourly'},
    {name: 'Pages'},
    {name: 'Km'}
  ];

  test: string[] = [];
  constructor(private upperCasePipe: UpperCasePipe,
              private firmRatePersistenceManagementService: FirmRatePersistenceManagementService,
              private standardCostItemPersistenceManagement: StandardCostItemPersistenceManagement) { }

  ngOnInit() {
    console.log("\n\n\nSCI on page load: ", this.scis);
    console.log('Motion or Action: ', this.clientAction, ' Matter Type: ', this.mattertype);

  }

  ngDoCheck(): void {
    // console.log('\n\n*****Do Changes: ', this.mattertype);
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('/*//////Test', changes.mattertype, '  ', changes.clientAction);
    // console.log('\n\n*****Changes: ', this.mattertype);
    // console.log('\n\n*****clientAction: ', changes.clientAction.currentValue);
    // console.log('\n\n*****Changes: ', changes.mattertype.currentValue);
    // Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    // Add '${implements OnChanges}' to the class.
  }

  updateMatterType(mattertype) {
    this.matterType = mattertype;

  }

  containsAction(sci) {
    console.log('******Call motionAction Filter*****', this.motionAction);
    for (const scimot in (sci as StandardCostItem).motionAction) {
      console.log(scimot);
      if (scimot === this.motionAction) {
        return true;
      }
    }

    return false;
  }

  containsMatterType(sci) {
    console.log('******Call matterType Filter*****', this.matterType);

    for (const matter in (sci as StandardCostItem).motionAction) {
      if (matter === this.matterType) {
        return true;
      }
    }

    return false;
  }
  updateAction(actionMotion) {
    this.motionAction = actionMotion;

    for(let sci in this.scis) {
      // if(sci.motionAction.contains('\\b' + this.motionAction + '\\b')) {
      //   this.viewScis.push(sci as unknown as StandardCostItem);//TODO: Fix
      // }
    }


  }

  addPhasetolist(text: string) {

    this.test.push(this.upperCasePipe.transform(text));
    console.log('/n/n****************test arr: ', this.test);
    // alert(this.test);
  }

  changeSCIPhase(sciName, phase) {
    // function to update sci with a new phase as user selects to apply phase to sci on admin
    // get sci from database using its name

    this.standardCostItemPersistenceManagement.findOneSci(sciName);
    let updatedSci = new StandardCostItem();

    this.standardCostItemPersistenceManagement.findUniqueStandardCostItemResponseObservable.subscribe(
      sci => {
        updatedSci = sci.standardCostItem;
        updatedSci.matterPhases = [];
        updatedSci.matterPhases.push(phase);
        // call function to update sci
        this.standardCostItemPersistenceManagement.updateSCI(updatedSci);
      }
    );
  }

  changeSCIRate(sciName, rateName) {
    // function to update sci rate
    this.standardCostItemPersistenceManagement.findOneSci(sciName);
    let updatedSci = new StandardCostItem();

    this.standardCostItemPersistenceManagement.findUniqueStandardCostItemResponseObservable.subscribe(
      sci => {
        updatedSci = sci.standardCostItem;
        // find rate using its name and fucntion
        const findFirmRateRequest = new FindFirmRateRequest();
        findFirmRateRequest.firmRateName = rateName;
        this.firmRatePersistenceManagementService.findFirmRate(findFirmRateRequest);
        this.firmRatePersistenceManagementService.findFirmRateResponseObservable.subscribe(
          firmRateResponse => {

            updatedSci.rate = null;
            updatedSci.rate = firmRateResponse.firmRate;
            // call function to update sci
            this.standardCostItemPersistenceManagement.updateSCI(updatedSci);
          });
      });
  }

  getSelectedItem(name: string) {
    this.selectedName = name;
    alert('Selected Name: ' + this.selectedName);
  }

}
