import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRatesHeaderComponent } from './admin-rates-header.component';

describe('AdminRatesHeaderComponent', () => {
  let component: AdminRatesHeaderComponent;
  let fixture: ComponentFixture<AdminRatesHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRatesHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRatesHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
