import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Permission } from 'src/app/models/permission.model';
import { Role } from 'src/app/models/role.model';
import { FirmPersistenceManagement } from 'src/app/services/legal/firm-persistence/firm-persistence-management.service';
import { Firm } from 'src/app/models/firm.model';
import { RequestNotValidException } from 'src/app/services/helper-services/exception-handling/request-not-valid-exception.model';
import { UnexpectedServiceError } from 'src/app/services/helper-services/exception-handling/unexpected-service-error.model';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { FindFirmResponse } from 'src/app/services/legal/firm-persistence/models/find-firm-response.model';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsComponent implements OnInit, AfterViewInit {


  @Input() firmRef;
  firm: Firm;
  roles: Role[] = [];

  isAdmin = false;
  overWriteDefaultUOM = false;
  addAdditionalEmail = false;
  printQuote = false;
  reuseHistoryQuote = false;

  constructor(private firmInSessionService: FirmInSessionService,
              private firmPersistenceManagement: FirmPersistenceManagement) { }

  ngOnInit() {
    this.firmInSessionService.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);
    this.firmPersistenceManagement.findFirm(this.firmRef);

    setTimeout(() => {
        this.firmPersistenceManagement.findFirmObservable.subscribe(response => {
          if (response instanceof FindFirmResponse) {
            this.firm = response.firm;
            this.roles = (this.firm as Firm).roles;
            // this.firmLoaded = true;
          }
        });
      // this.customLoaderService.updateLoaderState(false);
    }, 2000);
  }

  ngAfterViewInit() {




    this.firmPersistenceManagement.findFirmException.subscribe(exception => {
      if (exception instanceof RequestNotValidException) {
        // handle errors
      }
      if (exception instanceof UnexpectedServiceError) {
        // handle errors
      } else {
        // default error catch
      }

    });
  }


  addPermission(permission: any, role: string) {
    switch (permission) {
      case 'OVERWRITERATES': {

        break;
      }
      case 'OVERWRITEUOM': {
// tslint:disable-next-line: max-line-length
        // this.overWriteDefaultUOM = (this.overWriteDefaultUOM === false) ? this.overWriteDefaultUOM = true : this.overWriteDefaultUOM = false;
        //this.role.permissions.push('OVERWRITEUOM');
        (this.firm as Firm).roles.forEach(tempRole => {
          if ((tempRole as Role).name === role) {
            tempRole.permission.overWriteDefaultUOM = true;
          }
        });

        console.log('Right: ', this.overWriteDefaultUOM);
        break;
      }
      case 'OVERWRITECALCULATEDCOST': {
        break;
      }
      case 'OVERWRITEEMAIL': {
        break;
      }
      case 'ADDITIONALEMAIL': {
        (this.firm as Firm).roles.forEach(tempRole => {
          if ((tempRole as Role).name === role) {
            tempRole.permission.addAdditionalEmail = true;
          }
        });

        this.addAdditionalEmail = (this.addAdditionalEmail === false) ? this.addAdditionalEmail = true : this.addAdditionalEmail = false;

        console.log('Right: ', this.printQuote);
        break;
      }
      case 'PRINTQUOTE': {
        (this.firm as Firm).roles.forEach(tempRole => {
          if ((tempRole as Role).name === role) {
            tempRole.permission.printQuote = true;
          }
        });

        this.printQuote = (this.printQuote === false) ? this.printQuote = true : this.printQuote = false;
        console.log('Right: ', this.printQuote);
        break;
      }
      case 'REUSEQUOTE': {
        (this.firm as Firm).roles.forEach(tempRole => {
          if ((tempRole as Role).name === role) {
            tempRole.permission.reuseHistoryQuote = true;
          }
        });

        this.reuseHistoryQuote = (this.reuseHistoryQuote === false) ? this.reuseHistoryQuote = true : this.reuseHistoryQuote = false;
        console.log('Right: ', this.reuseHistoryQuote);
        break;
      }
    }

    this.firmPersistenceManagement.updateFirm(this.firm as Firm);

  }

}
