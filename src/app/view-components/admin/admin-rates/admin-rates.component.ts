import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-admin-rates',
  templateUrl: './admin-rates.component.html',
  styleUrls: ['./admin-rates.component.css']
})
export class AdminRatesComponent implements OnInit {

  @Input() firmRef;
  @Input() firm;
  constructor() { }

  ngOnInit() {
  }

}
