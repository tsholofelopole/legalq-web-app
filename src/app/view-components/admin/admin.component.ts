import { Component, OnInit, DoCheck } from '@angular/core';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { Firm } from 'src/app/models/firm.model';
import { FirmPersistenceManagement } from 'src/app/services/legal/firm-persistence/firm-persistence-management.service';
import { FindFirmResponse } from 'src/app/services/legal/firm-persistence/models/find-firm-response.model';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';

// such override allows to keep some initial values
export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  oneAtATime = true;
  firmRef = '';
  firm: Firm;
  firmLoaded = false;

  constructor(private firmInSession: FirmInSessionService,
              private firmPersistence: FirmPersistenceManagement,
              private customLoaderService: CustomLoaderService) { }

  ngOnInit() {
    this.firmInSession.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);
    this.firmPersistence.findFirm(this.firmRef);
    this.firmPersistence.findFirmObservable.subscribe(response => {
      if (response instanceof FindFirmResponse) {
        this.firm = response.firm;
        this.firmLoaded = true;

        setTimeout(() => {
          this.customLoaderService.updateLoaderState(false);
        }, 2000);

      }
      // this.customLoaderService.updateLoaderState(false);
    });
  }

}
