import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmClausesComponent } from './firm-clauses.component';

describe('FirmClausesComponent', () => {
  let component: FirmClausesComponent;
  let fixture: ComponentFixture<FirmClausesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirmClausesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmClausesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
