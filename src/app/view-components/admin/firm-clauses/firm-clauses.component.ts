import { Component, OnInit, Input } from '@angular/core';
import { Clause } from 'src/app/models/clause.model';
import { FirmPersistenceManagement } from 'src/app/services/legal/firm-persistence/firm-persistence-management.service';
import { Firm } from 'src/app/models/firm.model';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { Client } from './../../../models/client.model';
import { License } from 'src/app/models/license.model';
import { LicenseType } from 'src/app/models/license-type.model';
import { Role } from 'src/app/models/role.model';
import { Permission } from './../../../models/permission.model';
import { QuoteHistory } from 'src/app/models/quote-history.model';
import { MatterType } from './../../../models/matter-type.model';
import { SystemUser } from './../../../models/system-user.model';
import { FindFirmResponse } from 'src/app/services/legal/firm-persistence/models/find-firm-response.model';

@Component({
  selector: 'app-firm-clauses',
  templateUrl: './firm-clauses.component.html',
  styleUrls: ['./firm-clauses.component.css']
})
export class FirmClausesComponent implements OnInit {

  //@Input() clauses;
  @Input() firmRef;
  firm: Firm;

  clauses: Clause[] = [];
  clause = new Clause();

  clauseName = '';
  clauseDescription = '';
  clauseState = false;
  errmsg = '';

  clicked = false;
  constructor(private firmPersistanceManagement: FirmPersistenceManagement,
              private firmInSession: FirmInSessionService ) { }

  ngOnInit() {
    this.firmInSession.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);
    this.firmPersistanceManagement.findFirm(this.firmRef);

    setTimeout(() => {
        this.firmPersistanceManagement.findFirmObservable.subscribe(response => {
          if (response instanceof FindFirmResponse) {
            this.firm = response.firm;
            // this.firmLoaded = true;
            this.clauses = this.firm.clauses;
          }
        });
      // this.customLoaderService.updateLoaderState(false);
    }, 2000);

  }

  insertClause() {

    const tempFirm = new Firm();

    tempFirm.address = (this.firm as Firm).address;
    tempFirm.legalName = (this.firm as Firm).legalName;
    tempFirm.legalReference = (this.firm as Firm).legalReference;
    tempFirm.quoteReferencesSeq = (this.firm as Firm).quoteReferencesSeq;
    tempFirm.headerImage = (this.firm as Firm).headerImage;
    tempFirm.footerImage = (this.firm as Firm).footerImage;

    // map clients
    (this.firm as Firm).clients.forEach( client => {
      const tempClient = new Client();
      tempClient.clientEmail = client.clientEmail;
      tempClient.clientName = client.clientName;

      tempFirm.clients.push(tempClient);
    });

    // map licenses
    (this.firm as Firm).licenses.forEach( license => {
      const tempLicense = new License();
      tempLicense.noOfUsers = license.noOfUsers;
      tempLicense.expiryDate = license.expiryDate;
      tempLicense.active = license.active;

      const licenseType = new LicenseType();
      licenseType.name = license.licenseType.name;
      licenseType.status = license.licenseType.status;

      tempLicense.licenseType = licenseType;

      tempFirm.licenses.push(tempLicense);
    });

    // map roles
    (this.firm as Firm).roles.forEach( role => {

      const tempRole = new Role();
      tempRole.name = role.name;
      tempRole.counsel = role.counsel;
      tempRole.status = role.status;
      tempRole.hourlyRate = role.hourlyRate;
      tempRole.kmRate = role.kmRate;
      tempRole.fifteenMinIntervalRate = role.fifteenMinIntervalRate;

      const tempPerm = new Permission();
      tempPerm.isAdmin = role.permission.isAdmin;
      tempPerm.overWriteDefaultUOM = role.permission.overWriteDefaultUOM;
      tempPerm.addAdditionalEmail = role.permission.addAdditionalEmail;
      tempPerm.printQuote = role.permission.printQuote;
      tempPerm.reuseHistoryQuote = role.permission.reuseHistoryQuote;
      tempRole.permission = tempPerm;

      tempFirm.roles.push(tempRole);
    });

    // map clause
    (this.firm as Firm).clauses.forEach( clause => {
      const tempClause = new Clause();
      tempClause.name = clause.name;
      tempClause.clause = clause.clause;
      tempClause.clauseState = clause.clauseState;

      tempFirm.clauses.push(tempClause);

    });

    this.clicked = true;
    // insert clause to existsing firm's clauses
    if (this.clauseDescription === '' || this.clauseName === '') {
      // display error message, clause name and description must be set
      this.clicked = false;
      this.errmsg = 'Clause name and description must be specified.';

    } else {
      // alert('NBew Clause: ' + this.clauseName + ', ' + this.clauseDescription + ', ' + this.clauseState);
      // decide if function to get function should be called
      const clause = new Clause();
      clause.name = this.clauseName;
      clause.clause = this.clauseDescription;
      clause.clauseState = this.clauseState ? 'ACTIVE' : 'NOTACTIVE';
      tempFirm.clauses.push(clause);
      // this.firm.clauses.push(clause);
      this.firmPersistanceManagement.updateFirm(tempFirm);
      this.errmsg = '';
    }

  }

}
