import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Role } from 'src/app/models/role.model';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { FirmPersistenceManagement } from 'src/app/services/legal/firm-persistence/firm-persistence-management.service';
import { Firm } from 'src/app/models/firm.model';
import { RequestNotValidException } from 'src/app/services/helper-services/exception-handling/request-not-valid-exception.model';
import { UnexpectedServiceError } from 'src/app/services/helper-services/exception-handling/unexpected-service-error.model';
import { Permission } from 'src/app/models/permission.model';
import { FindFirmResponse } from 'src/app/services/legal/firm-persistence/models/find-firm-response.model';

@Component({
  selector: 'app-firm-roles-details',
  templateUrl: './firm-roles-details.component.html',
  styleUrls: ['./firm-roles-details.component.css']
})
export class FirmRolesDetailsComponent implements OnInit, AfterViewInit {

  @Input() firmRef;

  roles: Role[] = [];
  role = new Role();
  errMsg = '';
  firm: Firm;

  // two way binding objects
  roleName = '';
  kmRate = 0;
  hourlyRate = 0;
  fifteenMinIntervalRate = 0;
  roleStatus = false;

  clicked = false;

  constructor(private firmInSessionService: FirmInSessionService,
              private firmPersistenceManagement: FirmPersistenceManagement,
              ) { }

  ngOnInit() {
    this.firmInSessionService.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);
    this.firmPersistenceManagement.findFirm(this.firmRef);

    setTimeout(() => {
        this.firmPersistenceManagement.findFirmObservable.subscribe(response => {
          if (response instanceof FindFirmResponse) {
            this.firm = response.firm;
            this.roles = (this.firm as Firm).roles;
            // this.firmLoaded = true;
          }
        });
      // this.customLoaderService.updateLoaderState(false);
    }, 2000);

  }

  ngAfterViewInit() {

    this.firmPersistenceManagement.findFirmException.subscribe(exception => {
      if (exception instanceof RequestNotValidException) {
        // handle errors
      }
      if (exception instanceof UnexpectedServiceError) {
        // handle errors
      } else {
        // default error catch
      }

    });
  }

  insertRole() {
    if (this.roleName === '') {
        this.errMsg = 'Role name must be specified.';
    }

    let newRole = new Role();
    newRole.name = this.roleName;
    newRole.status = this.roleStatus ? 'ACTIVE' : 'INACTIVE';
    newRole.kmRate = this.kmRate;
    newRole.hourlyRate = this.hourlyRate;
    newRole.fifteenMinIntervalRate = this.hourlyRate / 4;

    const rolePermission = new Permission();
    rolePermission.addAdditionalEmail = false;
    rolePermission.reuseHistoryQuote = false;
    rolePermission.printQuote = false;
    rolePermission.overWriteDefaultUOM = false;
    rolePermission.isAdmin = false;

    newRole.permission = rolePermission;

    //do the update at this point
    (this.firm as Firm).roles.push(newRole);
    this.firmPersistenceManagement.updateFirm((this.firm as Firm));
  }

}
