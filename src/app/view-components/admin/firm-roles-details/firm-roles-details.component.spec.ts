import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmRolesDetailsComponent } from './firm-roles-details.component';

describe('FirmRolesDetailsComponent', () => {
  let component: FirmRolesDetailsComponent;
  let fixture: ComponentFixture<FirmRolesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirmRolesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmRolesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
