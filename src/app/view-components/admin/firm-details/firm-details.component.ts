import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { FirmPersistenceManagement } from 'src/app/services/legal/firm-persistence/firm-persistence-management.service';
import { Firm } from 'src/app/models/firm.model';
import { Exception } from 'src/app/services/helper-services/exception-handling/exception.model';
import { RequestNotValidException } from 'src/app/services/helper-services/exception-handling/request-not-valid-exception.model';
import { UnexpectedServiceError } from 'src/app/services/helper-services/exception-handling/unexpected-service-error.model';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { FindFirmResponse } from 'src/app/services/legal/firm-persistence/models/find-firm-response.model';

@Component({
  selector: 'app-firm-details',
  templateUrl: './firm-details.component.html',
  styleUrls: ['./firm-details.component.css']
})
export class FirmDetailsComponent implements OnInit, AfterViewInit {

  @Input() firmRef;

  firm: Firm;
  clicked = false;
  firmName = '';
  firmLegalReference = '';
  firmAddress = '';

  constructor(private firmPersistenceManagement: FirmPersistenceManagement,
              private firmInSessionService: FirmInSessionService) { }

  ngOnInit() {

    this.firmInSessionService.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);
    this.firmPersistenceManagement.findFirm(this.firmRef);

    setTimeout(() => {
        this.firmPersistenceManagement.findFirmObservable.subscribe(response => {
          if (response instanceof FindFirmResponse) {
            this.firm = response.firm;
            // this.firmLoaded = true;
          }
        });
      // this.customLoaderService.updateLoaderState(false);
    }, 2000);
  }

  ngAfterViewInit() {

    this.firmPersistenceManagement.findFirmException.subscribe(exception => {
      if (exception instanceof RequestNotValidException) {
        // handle errors
      }
      if (exception instanceof UnexpectedServiceError) {
        // handle errors
      } else {
        // default error catch
      }

    });
  }

  updatefirmDetails() {
    // to be implemented after update method exists on the service
    this.firm.address = this.firmAddress;
    this.firm.legalName = this.firmName;
    this.firm.legalReference = this.firmLegalReference;

    this.firmPersistenceManagement.updateFirm(this.firm);
  }

}
