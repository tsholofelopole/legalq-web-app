import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRatesContentComponent } from './admin-rates-content.component';

describe('AdminRatesContentComponent', () => {
  let component: AdminRatesContentComponent;
  let fixture: ComponentFixture<AdminRatesContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRatesContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRatesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
