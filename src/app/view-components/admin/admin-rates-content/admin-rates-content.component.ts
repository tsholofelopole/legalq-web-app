import { Component, OnInit, Input } from '@angular/core';
import { FirmRate } from 'src/app/models/firm-rate.model';
import { FirmRatePersistenceManager } from 'src/app/services/legal/firm-rate-persistence/http/firm-rate-persistence-manager.service';
import { FindFirmRateRequest } from './../../../services/legal/firm-rate-persistence/models/find-firm-rate-request.model';
import { FindFirmRatesRequest } from 'src/app/services/legal/firm-rate-persistence/models/find-firm-rates-request.model';
// tslint:disable-next-line: max-line-length
// tslint:disable-next-line: import-spacing
import { FirmRatePersistenceManagementService } from
'src/app/services/legal/firm-rate-persistence/firm-rate-persistence-management.service';
import { FindFirmRatesResponse } from 'src/app/services/legal/firm-rate-persistence/models/find-firm-reates-response.models';
import { FirmInSessionService } from 'src/app/services/helper-services/firm-in-session.service';
import { FindUOMRequest } from './../../../services/legal/firm-rate-persistence/models/find-uom-request.model';
import { FindUOMResponse } from './../../../services/legal/firm-rate-persistence/models/find-uom-response.model';
import { UnitOfMeasurement } from './../../../models/unit-of-measurement.model';
import { InsertFirmRateRequest } from 'src/app/services/legal/firm-rate-persistence/models/insert-firm-rate-request.model';
import { InsertFirmRateResponse } from 'src/app/services/legal/firm-rate-persistence/models/insert-firm-rate-response.model';


@Component({
  selector: 'app-admin-rates-content',
  templateUrl: './admin-rates-content.component.html',
  styleUrls: ['./admin-rates-content.component.css']
})
export class AdminRatesContentComponent implements OnInit {

  @Input() firmRef;

  rates: FirmRate[] = [];
  rate = new FirmRate();
  clicked = false;

  rateName = '';
  rateUom = '';
  rateValue = '';
  rateStartDate = new Date();
  rateEndDate = new Date();
  rateActive = false;
  errMsg = '';

  constructor(private firmInSessionService: FirmInSessionService,
              private firmRateService: FirmRatePersistenceManagementService) { }

  uoms = [
    {name: 'Page', value: 'Page'},
    {name: 'Copies', value: 'Copies'},
    {name: 'Unit', value: 'Unit'},
    {name: 'R', value: 'South African Rands'},
    {name: '%', value: 'Percentage'},
  ];

  ngOnInit() {
    this.firmInSessionService.currentFirmLegalReference.subscribe(ref => this.firmRef = ref);

    const findFirmRatesReq = new FindFirmRatesRequest();
    findFirmRatesReq.legalFirmRef = this.firmRef;

    this.firmRateService.findFirmRates(findFirmRatesReq);
    setTimeout(() => {
      this.firmRateService.findFirmRatesResponseObservable.subscribe(
        (response) => {
          console.log('\n\n\nReposne: ', response);
          if (response instanceof FindFirmRatesResponse) {
            this.rates = response.firmRate;
          }
        }
      );
    }, 2000);
  }

  updteRateUOM(uom) {

  }

  makeRateActive(rateName) {

  }

  insertRate() {
    if (this.rateName === '' || this.rateValue === '') {
      this.errMsg = 'Rate name and value are required';
    }

    const findUomReq = new FindUOMRequest();
    findUomReq.uomName = this.rateUom.toUpperCase();
    this.firmRateService.findUom(findUomReq);

    setTimeout(() => {
      this.firmRateService.findUomResponseObservable.subscribe(
        (response) => {
          if (response instanceof FindUOMResponse) {

            const firmRate = new FirmRate();
            firmRate.endDate = this.rateEndDate;
            firmRate.startDate = this.rateStartDate;
            firmRate.firmLegalRef = this.firmRef;
            firmRate.name = this.rateName;

            const uom = new UnitOfMeasurement();
            uom.name = response.uom.name;
            uom.description = response.uom.description;
            uom.code = response.uom.code;
            uom.active = response.uom.active;
            firmRate.unitOfMeasurement = uom;

            const insertFirmRateReq = new InsertFirmRateRequest();
            insertFirmRateReq.firmRate = firmRate;
            this.firmRateService.insertFirmRate(insertFirmRateReq);

            this.ngOnInit();

            this.firmRateService.insertFirmRateResponseObservable.subscribe(
              (response) => {
                if (response instanceof InsertFirmRateResponse) {
                    this.rateName = '';
                    this.rateUom = '';
                    this.rateValue = '';
                    this.rateStartDate = new Date();
                    this.rateEndDate = new Date();
                    this.rateActive = false;
                    this.errMsg = '';
                }
              }
            );

          }
        }
      );
    }, 2000);

  }
  // SORTING
  // this.transactions = this.transactions.sort((a, b) => {
  //   return <any>new Date(b.transactionDate) - <any>new Date(a.transactionDate);
  // });
  // <p class="transactionDate">{{transaction.transactionDate | date: 'yyyy-MM-dd' }}</p>
}
