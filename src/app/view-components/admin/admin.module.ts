import { NgModule } from '@angular/core';
import { CommonModule, UpperCasePipe } from '@angular/common';
import { AdminComponent } from './admin.component';
import { HeaderModule } from '../header/header.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { FirmDetailsComponent } from './firm-details/firm-details.component';
import { LegalModule } from 'src/app/services/legal/legal.module';
import { FirmTemplateComponent } from './firm-template/firm-template.component';
import { FirmRolesDetailsComponent } from './firm-roles-details/firm-roles-details.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { FirmClausesComponent } from './firm-clauses/firm-clauses.component';
import { AdminSciComponent } from './admin-sci/admin-sci.component';
import { AdminSciHeaderComponent } from './admin-sci-header/admin-sci-header.component';
import { AdminSciContentComponent } from './admin-sci-content/admin-sci-content.component';
import { AdminRatesHeaderComponent } from './admin-rates-header/admin-rates-header.component';
import { AdminRatesContentComponent } from './admin-rates-content/admin-rates-content.component';
import { AdminRatesComponent } from './admin-rates/admin-rates.component';
import { DocumentTemplateManagerService } from 'src/app/services/view-services/document-template-manager.service';
import { QuoteDocumentModule } from '../quote-document/quote-document.module';
import { DisplayQuoteManager } from 'src/app/services/view-services/display-quote-manager.service';
import { HelperServicesModule } from 'src/app/services/helper-services/helper-services.module';
import { CustomLoaderService } from 'src/app/services/view-services/cuctom-loader.service';
import { OtherLoaderModule } from './../other-loader/other-loader.module';

@NgModule({
  declarations: [AdminComponent,
    FirmDetailsComponent,
    FirmTemplateComponent,
    FirmRolesDetailsComponent,
    PermissionsComponent,
    FirmClausesComponent,
    AdminSciComponent,
    AdminSciHeaderComponent,
    AdminSciContentComponent,
    AdminRatesHeaderComponent,
    AdminRatesContentComponent,
    AdminRatesComponent],
  imports: [
  CommonModule,
    HeaderModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AccordionModule.forRoot(),
    LegalModule,
    QuoteDocumentModule,
    HelperServicesModule,
    OtherLoaderModule
  ],
  exports: [AdminComponent,
     FirmRolesDetailsComponent,
      PermissionsComponent,
      FirmClausesComponent,
      AdminSciComponent,
      AdminSciHeaderComponent,
      AdminSciContentComponent],
  providers: [
    UpperCasePipe,
    DocumentTemplateManagerService,
    DisplayQuoteManager,
    CustomLoaderService
  ]

})
export class AdminModule { }
