import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSciComponent } from './admin-sci.component';

describe('AdminSciComponent', () => {
  let component: AdminSciComponent;
  let fixture: ComponentFixture<AdminSciComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSciComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
