import { Component, OnInit, Input, DoCheck } from '@angular/core';
import { UpperCasePipe } from '@angular/common';
import { FindFirmRateRequest } from 'src/app/services/legal/firm-rate-persistence/models/find-firm-rate-request.model';
// tslint:disable-next-line: max-line-length
import { FirmRatePersistenceManagementService } from 'src/app/services/legal/firm-rate-persistence/firm-rate-persistence-management.service';
import { FirmRate } from 'src/app/models/firm-rate.model';
import { StandardCostItem } from './../../../models/standard-cost-item.model';
import { StandardCostItemPersistenceManagement } from 'src/app/services/legal/standard-cost-item-persistence/standard-cost-item-persistence-management.service';
import { UnitOfMeasurement } from 'src/app/models/unit-of-measurement.model';
import { FindFirmRateResponse } from './../../../services/legal/firm-rate-persistence/models/find-firm-rate-response.model';
import { FindStandardCostItemResponse } from './../../../services/legal/standard-cost-item-persistence/models/find-standard-cost-item-response.model';
import { InsertFirmRateResponse } from 'src/app/services/legal/firm-rate-persistence/models/insert-firm-rate-response.model';
import { InsertStandardCostItemResponse } from 'src/app/services/legal/standard-cost-item-persistence/models/insert-standard-cost-item-response.model';

@Component({
  selector: 'app-admin-sci',
  templateUrl: './admin-sci.component.html',
  styleUrls: ['./admin-sci.component.css']
})
export class AdminSciComponent implements OnInit, DoCheck {

  constructor(private upperCasePipe: UpperCasePipe,
              private standardCostItemPersistenceManagement: StandardCostItemPersistenceManagement,
              private firmRatePersistenceManagementService: FirmRatePersistenceManagementService) { }

  clicked = false;

  // filters
  motionAction = '';
  matterType = '';
  scis: StandardCostItem[] = [];

  matterActionMotion = [
    {name: 'ACTION'},
    {name: 'MOTION'}
  ];

  sciCategories = [
    {name: 'PLANTIFF'},
    {name: 'APPLICANT'},
    {name: 'DEFENDANT'},
    {name: 'RESPONDENT'}
  ];

  matterTypes = [
    {name: 'RAF'},
    {name: 'SUMMONS'},
    {name: 'DIVORCE'},
    {name: 'LIQUIDATION'},
    {name: 'INTERLOCULARY'},
    {name: 'OTHER'}
  ];

  trialPhases = [
    {name: 'Assessment/Development'},
    {name: 'Pleadings'},
    {name: 'Discovery'},
    {name: 'Matter Defended'},
    {name: 'Trial preparation and Trial'}
  ];

  rates = [
    {name: 'Km'},
    {name: 'Hourly'},
    {name: 'Page'},
    {name: 'Drafting - Notice, Pages'},
    {name: 'Drafting Letters'},
    {name: 'Drafting - Pleading'},
    {name: 'Perusal'},
    {name: 'Copies'},
    {name: 'Brief'},
    {name: 'Cheque'},
    {name: 'Execution'},
    {name: 'Re-issue'},
    {name: 'Call Rate'}
  ];

  uoms = [
    {name: 'Hourly'},
    {name: 'Pages'},
    {name: 'Km'}
  ];

  test: string[] = [];
  sciName = '';
  sciSequence = 0;
  sciDescription = '';
  sciPhase = '';
  sciState = false;
  sciRateName = '';
  sciRate = new FirmRate();
  sciCategory = '';
  responseMsg = '';

  ngOnInit() {
    this.standardCostItemPersistenceManagement.findSci();

    this.standardCostItemPersistenceManagement.findStandardCostItemResponseObservable.subscribe(
      response => {
        if (response instanceof FindStandardCostItemResponse) {
          this.scis = response.standardCostItems;
        }
      }
    );
  }

  ngDoCheck(): void {
    this.standardCostItemPersistenceManagement.findStandardCostItemResponseObservable.subscribe(
      response => {
        if (response instanceof FindStandardCostItemResponse) {
          this.scis = response.standardCostItems;
        }
      }
    );

  }


  addPhasetolist(text) {
    this.sciPhase = text.target.value;
  }

  setSCIRate() {
    const findFirmRateRequest = new FindFirmRateRequest();
    findFirmRateRequest.firmRateName = this.sciRateName.toUpperCase();
    this.firmRatePersistenceManagementService.findFirmRate(findFirmRateRequest);
  }


 insertSci() {
   if (this.sciName === '' ||
   this.sciSequence === 0 ||
   this.sciPhase === '') {
    this.responseMsg = 'Stadard Cost Item name, sequence and Phase are required.';
   } else {
    this.setSCIRate();

    setTimeout(() => {
       this.firmRatePersistenceManagementService.findFirmRateResponseObservable.subscribe(
         response => {
           if (response instanceof FindFirmRateResponse) {

             console.log('\n\n$$$$$$find rate response: ', (response as FindFirmRateResponse).firmRate, typeof response);
             this.sciRate = response.firmRate;
             if (response.firmRate instanceof FirmRate) {
               alert('SAME!!!!!!!!!');
             }
             console.log('**********\n\nfirmrate: ', this.sciRate);

             const standardcostitem = new StandardCostItem();
             standardcostitem.name = this.sciName;
             standardcostitem.description = '';
             standardcostitem.seqNumber = this.sciSequence;
             standardcostitem.canEmployCounsel = false;
             standardcostitem.canHaveDisbursement = false;
             standardcostitem.matterPhases.push(this.sciPhase);
             standardcostitem.matterTypes.push(this.matterType);

             switch (this.sciCategory) {
               case 'PLAINTIFF':
                   standardcostitem.motionAction.push('ACTION');
                   break;
               case 'DEFENDANT':
                   standardcostitem.motionAction.push('MOTION');
                   break;
               case 'RESPONDANT':
                   standardcostitem.motionAction.push('MOTION');
                   break;
               case 'APPLICANT':
                   standardcostitem.motionAction.push('ACTION');
                   break;
             }

             const firmRate = new FirmRate();
             firmRate.name = (response.firmRate as FirmRate).name;
             firmRate.firmLegalRef = (response.firmRate as FirmRate).firmLegalRef;
             firmRate.rate = (response.firmRate as FirmRate).rate;
             firmRate.startDate = (response.firmRate as FirmRate).startDate;
             firmRate.endDate = (response.firmRate as FirmRate).endDate;
             firmRate.active = (response.firmRate as FirmRate).active;

             const uom = new UnitOfMeasurement();
             uom.name = (response.firmRate as FirmRate).unitOfMeasurement.name;
             uom.description = (response.firmRate as FirmRate).unitOfMeasurement.description;
             uom.code = (response.firmRate as FirmRate).unitOfMeasurement.code;
             uom.active = (response.firmRate as FirmRate).unitOfMeasurement.active;
             firmRate.unitOfMeasurement = uom;

             standardcostitem.rate = firmRate;

             console.log('SCI: ', standardcostitem);

             this.standardCostItemPersistenceManagement.insertSci(standardcostitem);
           // });
             console.log('\n\nLeaving set SCI rate\n\n');

             console.log('The firm rate to be inserted: ', this.sciPhase, ' ', this.matterType, ' ',  this.sciRate, ' ', this.sciName);

           }

       });

       this.standardCostItemPersistenceManagement.findSci();

       this.standardCostItemPersistenceManagement.insertStandardCostItemResponseObservable.subscribe(
         response => {
           if (response instanceof InsertStandardCostItemResponse) {
             if (response !== null) {
               this.responseMsg = 'Inserted SCI successfully!!!';
               this.ngOnInit();
               this.sciName = '';
               this.sciSequence = 0;
               this.sciDescription = '';
               this.sciPhase = '';
               // sciRate = '';
               this.sciState = false;
               this.sciRateName = '';
               this.sciRate = new FirmRate();
               this.sciCategory = '';
             }
           }
         }
       );

       this.standardCostItemPersistenceManagement.findStandardCostItemResponseObservable.subscribe(
         response => {
           if (response instanceof FindStandardCostItemResponse) {
             this.scis = response.standardCostItems;
           }
         }
       );


     }, 5000);

  }

}
}
