import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MotionTypeManagerService } from 'src/app/services/view-services/motion-type-manager.service';
import { UserStoryLineManagerService } from 'src/app/services/view-services/user-story-line-manager.service';
import { ClientTypeManagement } from 'src/app/services/view-services/client-type-management.service';

@Component({
  selector: 'app-mattertype',
  templateUrl: './mattertype.component.html',
  styleUrls: ['./mattertype.component.css']
})
export class MattertypeComponent implements OnInit {

  constructor(private router: Router,
              private motionTypeManagerService: MotionTypeManagerService,
              private userStoryLineManagerService: UserStoryLineManagerService,
              private clientTypeManagement: ClientTypeManagement) { }

  ngOnInit() {
  }

  setPlantiff() {
    this.clientTypeManagement.updateClientType('PLANTIFF');
    this.userStoryLineManagerService.updateStoryLine('Action');
    this.router.navigate(['/quotetype']);
  }

  setRespondant() {
    this.clientTypeManagement.updateClientType('RESPONDANT');
    this.userStoryLineManagerService.updateStoryLine('Motion');
    this.router.navigate(['/quotetype']);
  }

}
