import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MattertypeComponent } from './mattertype.component';
import { HeaderModule } from '../header/header.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { MotionTypeManagerService } from 'src/app/services/view-services/motion-type-manager.service';
import { UserStoryLineManagerService } from 'src/app/services/view-services/user-story-line-manager.service';
import { ClientTypeManagement } from 'src/app/services/view-services/client-type-management.service';

@NgModule({
  declarations: [MattertypeComponent],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    HeaderModule
  ],
  exports: [MattertypeComponent],
  providers: [MotionTypeManagerService,
  UserStoryLineManagerService,
  ClientTypeManagement]
})
export class MattertypeModule { }
