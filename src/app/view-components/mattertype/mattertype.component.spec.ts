import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MattertypeComponent } from './mattertype.component';

describe('MattertypeComponent', () => {
  let component: MattertypeComponent;
  let fixture: ComponentFixture<MattertypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MattertypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MattertypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
