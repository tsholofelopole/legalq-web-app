import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserStoryLineManagerService } from 'src/app/services/view-services/user-story-line-manager.service';

@Component({
  selector: 'app-quotetype',
  templateUrl: './quotetype.component.html',
  styleUrls: ['./quotetype.component.css']
})
export class QuotetypeComponent implements OnInit {

  constructor(private router: Router,
              private userStoryLineManagerService: UserStoryLineManagerService) { }

  ngOnInit() {
  }

  setQuoteType(mattertype, description) {
    // call service to set the request to get the relative cost items for the mattertype and description
    console.log('Matter type: ', mattertype, ' : ', description);

    this.userStoryLineManagerService.updateStoryLine(mattertype);
    this.userStoryLineManagerService.updateStoryLine(description);

    this.router.navigate(['/quote']);

  }

}
