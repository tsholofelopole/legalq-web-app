import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuotetypeComponent } from './quotetype.component';
import { HeaderModule } from '../header/header.module';
import { UserStoryLineManagerService } from 'src/app/services/view-services/user-story-line-manager.service';

@NgModule({
  declarations: [QuotetypeComponent],
  imports: [
    CommonModule,
    HeaderModule
  ],
  exports: [QuotetypeComponent],
  providers: [UserStoryLineManagerService]
})
export class QuotetypeModule { }
