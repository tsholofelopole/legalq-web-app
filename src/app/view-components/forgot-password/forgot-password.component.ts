import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  resetpasswordForm: FormGroup;
  clicked = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router) { }


  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.resetpasswordForm = this.formBuilder.group({
      newpassword: ['', [
        Validators.required
      ]],
      confirmpassword: ['', [
        Validators.required
      ]]
    });
  }

  resetPassword() {
    // call service to reset password
  }

  goToHomeScreen() {
    this.router.navigate(['/login']);
  }

}
