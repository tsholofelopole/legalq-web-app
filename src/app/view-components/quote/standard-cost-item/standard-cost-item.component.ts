import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-standard-cost-item',
  templateUrl: './standard-cost-item.component.html',
  styleUrls: ['./standard-cost-item.component.css']
})
export class StandardCostItemComponent implements OnInit {


    standardCostItems = [
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 122333,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 122333,
      rate: 200,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 122333,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Pages',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 122333,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: false,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 122333,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: true,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 23,
      rate: 200,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: false,
      canEmployCounsel: false,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 2,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Pages',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: false,
      canEmployCounsel: false,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    },
    {
      '@class': 'org.legalq.legal.StandardCostItem',
      matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
      name: 'Consulation',
      description: 'Test SCI Object',
      seqNumber: 1,
      rate: 2000,
      unitOfMeasurement: {
        '@class': 'org.legalq.legal.UnitOfMeasurement',
        name: 'Hourly',
        description: 'Measurement value for Consultation hourly rate',
        code: 'B300',
        active: 2,
      },
      canHaveDisbursement: true,
      canEmployCounsel: false,
      active: 1,
      matterTypeEnum: null,
      disbFlag: false,
      matterPhaseInt: 1,
      quantity: 1
    }
  ];

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  constructor() {}
}
