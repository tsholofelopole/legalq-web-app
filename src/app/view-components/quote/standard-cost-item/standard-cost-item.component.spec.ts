import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardCostItemComponent } from './standard-cost-item.component';

describe('StandardCostItemComponent', () => {
  let component: StandardCostItemComponent;
  let fixture: ComponentFixture<StandardCostItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardCostItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardCostItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
