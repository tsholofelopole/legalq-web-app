import { Component, OnInit, Input, ViewChild, ViewContainerRef, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-sci-roles',
  templateUrl: './sci-roles.component.html',
  styleUrls: ['./sci-roles.component.css']
})
export class SciRolesComponent implements OnInit {

  //get all the roles when user logs in
  @Input() roles;
  @Input() sci;

  //what to clone
  @ViewChild('sciRolesRow', {static: false }) template:TemplateRef<any>;
  @ViewChild('rolescontainer', {static: false }) container: ViewContainerRef; // roles container

  selectedRole = '';

  constructor() { }

  ngOnInit() {

    this.roles = [
      {name: 'Manager'},
      {name: 'Director'},
      {name: 'Clerk'}
    ]
  }

  addRoleSci() {
    const template = this.template.createEmbeddedView(null);
    this.container.insert(template);
    // this.container.createEmbeddedView(this.template);
    console.log('adding role sci row');
  }

}
