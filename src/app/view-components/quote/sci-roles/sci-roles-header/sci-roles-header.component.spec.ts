import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SciRolesHeaderComponent } from './sci-roles-header.component';

describe('SciRolesHeaderComponent', () => {
  let component: SciRolesHeaderComponent;
  let fixture: ComponentFixture<SciRolesHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SciRolesHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SciRolesHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
