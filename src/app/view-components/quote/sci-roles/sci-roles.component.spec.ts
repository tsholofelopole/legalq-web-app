import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SciRolesComponent } from './sci-roles.component';

describe('SciRolesComponent', () => {
  let component: SciRolesComponent;
  let fixture: ComponentFixture<SciRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SciRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SciRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
