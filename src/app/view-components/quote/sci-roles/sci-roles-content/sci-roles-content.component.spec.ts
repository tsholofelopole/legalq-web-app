import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SciRolesContentComponent } from './sci-roles-content.component';

describe('SciRolesContentComponent', () => {
  let component: SciRolesContentComponent;
  let fixture: ComponentFixture<SciRolesContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SciRolesContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SciRolesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
