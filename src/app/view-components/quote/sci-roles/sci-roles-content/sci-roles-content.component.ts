import { Component, OnInit, Input, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-sci-roles-content',
  templateUrl: './sci-roles-content.component.html',
  styleUrls: ['./sci-roles-content.component.css']
})
export class SciRolesContentComponent implements OnInit {

  constructor() { }

   //get all the roles when user logs in
   @Input() roles;
   @Input() sci;

   //what to clone
  @ViewChild('sciRolesRow', {static: false }) template:TemplateRef<any>;
@ViewChild('rolescontainer', {static: false }) container: ViewContainerRef; // roles container

  ngOnInit() {
    this.roles = [
      {name: 'Manager'},
      {name: 'Director'},
      {name: 'Clerk'}
    ]
  }

  addRoleSci() {
    const template = this.template.createEmbeddedView(null);
    this.container.insert(template);
    // this.container.createEmbeddedView(this.template);
    console.log('adding role sci row');
  }

}
