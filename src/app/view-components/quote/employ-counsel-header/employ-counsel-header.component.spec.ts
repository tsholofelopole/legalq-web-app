import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployCounselHeaderComponent } from './employ-counsel-header.component';

describe('EmployCounselHeaderComponent', () => {
  let component: EmployCounselHeaderComponent;
  let fixture: ComponentFixture<EmployCounselHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployCounselHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployCounselHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
