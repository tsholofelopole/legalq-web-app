import { Component, OnInit, Input } from '@angular/core';
import { EmployCounselService } from 'src/app/services/view-services/employ-cousel.service';

@Component({
  selector: 'app-employ-counsel-header',
  templateUrl: './employ-counsel-header.component.html',
  styleUrls: ['./employ-counsel-header.component.css']
})
export class EmployCounselHeaderComponent implements OnInit {

  employCounsel = false;
  @Input() counselSci;
  constructor(private employCounselService: EmployCounselService) { }

  ngOnInit() {
  }

  updateCounsel() {
    if (this.employCounsel) {
      this.employCounsel = false;
      this.employCounselService.updateEmployCounselValue(this.employCounsel);
    } else {
      this.employCounsel = true;
      this.employCounselService.updateEmployCounselValue(this.employCounsel);
    }

  }

}
