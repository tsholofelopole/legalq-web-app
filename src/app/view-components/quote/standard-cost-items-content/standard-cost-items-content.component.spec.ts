import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardCostItemsContentComponent } from './standard-cost-items-content.component';

describe('StandardCostItemsContentComponent', () => {
  let component: StandardCostItemsContentComponent;
  let fixture: ComponentFixture<StandardCostItemsContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardCostItemsContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardCostItemsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
