import { Component, OnInit, Input } from '@angular/core';
import { SciRolesComponent } from '../sci-roles/sci-roles.component';

@Component({
  selector: 'app-standard-cost-items-content',
  templateUrl: './standard-cost-items-content.component.html',
  styleUrls: ['./standard-cost-items-content.component.css']
})
export class StandardCostItemsContentComponent implements OnInit {

  @Input() sci;
  fees = 0;

  uomIsHrs = false;

  constructor(private sciRolesComponent: SciRolesComponent) { }

  ngOnInit() {
    console.log("*******", this.sci);
  }

  addFees(fee) {
    this.fees = fee;
  }

  addRoleSci() {
    this.sciRolesComponent.addRoleSci();
  }

}
