import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SciHeaderComponent } from './sci-header.component';

describe('SciHeaderComponent', () => {
  let component: SciHeaderComponent;
  let fixture: ComponentFixture<SciHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SciHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SciHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
