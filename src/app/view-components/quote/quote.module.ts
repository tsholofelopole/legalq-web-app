import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuoteComponent } from './quote.component';
import { HeaderModule } from '../header/header.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { StandardCostItemComponent } from './standard-cost-item/standard-cost-item.component';
import { StandardCostItemsContentComponent } from './standard-cost-items-content/standard-cost-items-content.component';
import { SciRolesComponent } from './sci-roles/sci-roles.component';
import { SciRolesHeaderComponent } from './sci-roles/sci-roles-header/sci-roles-header.component';
import { SciRolesContentComponent } from './sci-roles/sci-roles-content/sci-roles-content.component';
import { EmployCounselComponent } from './employ-counsel/employ-counsel.component';
import { EmployCounselHeaderComponent } from './employ-counsel-header/employ-counsel-header.component';
import { EmployCounselService } from 'src/app/services/view-services/employ-cousel.service';
import { QuoteSummaryComponent } from './quote-summary/quote-summary.component';
import { QuoteSummaryScisComponent } from './quote-summary/quote-summary-scis/quote-summary-scis.component';
import { UserStoryLineManagerService } from 'src/app/services/view-services/user-story-line-manager.service';
import { SciHeaderComponent } from './sci-header/sci-header.component';

@NgModule({
  declarations: [
    QuoteComponent,
    StandardCostItemComponent,
    StandardCostItemsContentComponent,
    SciRolesComponent,
    SciRolesHeaderComponent,
    SciRolesContentComponent,
    EmployCounselComponent,
    EmployCounselHeaderComponent,
    QuoteSummaryComponent,
    QuoteSummaryScisComponent,
    SciHeaderComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AccordionModule.forRoot(),
  ],
  exports: [QuoteComponent,
    StandardCostItemComponent,
    StandardCostItemsContentComponent,
    SciRolesComponent,
    SciRolesHeaderComponent,
    SciRolesContentComponent,
    EmployCounselComponent,
    EmployCounselHeaderComponent,
    QuoteSummaryComponent,
    QuoteSummaryScisComponent,
    SciHeaderComponent
  ],
    providers: [
      SciRolesComponent,
      EmployCounselService,
      UserStoryLineManagerService
    ]
})
export class QuoteModule { }
