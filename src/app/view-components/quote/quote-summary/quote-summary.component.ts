import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quote-summary',
  templateUrl: './quote-summary.component.html',
  styleUrls: ['./quote-summary.component.css']
})
export class QuoteSummaryComponent implements OnInit {

  total = 15000;
  totalDisbursemnt = 1600;
  drawingFee = 200;
  VAT = this.total * 0.15;



  standardCositemSummaries = {
     scis : [
      {
        '@class': 'org.legalq.legal.StandardCostItem',
        matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
        name: 'Consulation',
        description: 'Test SCI Object',
        seqNumber: 122333,
        rate: 2000,
        unitOfMeasurement: {
          '@class': 'org.legalq.legal.UnitOfMeasurement',
          name: 'Hourly',
          description: 'Measurement value for Consultation hourly rate',
          code: 'B300',
          active: 2,
        },
        canHaveDisbursement: true,
        canEmployCounsel: true,
        active: 1,
        matterTypeEnum: null,
        disbFlag: false,
        matterPhaseInt: 1,
        quantity: 1
      },
      {
        '@class': 'org.legalq.legal.StandardCostItem',
        matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
        name: 'Consulation',
        description: 'Test SCI Object',
        seqNumber: 122333,
        rate: 200,
        unitOfMeasurement: {
          '@class': 'org.legalq.legal.UnitOfMeasurement',
          name: 'Hourly',
          description: 'Measurement value for Consultation hourly rate',
          code: 'B300',
          active: 2,
        },
        canHaveDisbursement: true,
        canEmployCounsel: true,
        active: 1,
        matterTypeEnum: null,
        disbFlag: false,
        matterPhaseInt: 1,
        quantity: 1
      },
      {
        '@class': 'org.legalq.legal.StandardCostItem',
        matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
        name: 'Consulation',
        description: 'Test SCI Object',
        seqNumber: 122333,
        rate: 2000,
        unitOfMeasurement: {
          '@class': 'org.legalq.legal.UnitOfMeasurement',
          name: 'Pages',
          description: 'Measurement value for Consultation hourly rate',
          code: 'B300',
          active: 2,
        },
        canHaveDisbursement: false,
        canEmployCounsel: true,
        active: 1,
        matterTypeEnum: null,
        disbFlag: false,
        matterPhaseInt: 1,
        quantity: 1
      },
      {
        '@class': 'org.legalq.legal.StandardCostItem',
        matterPhases: ['PRETRIAL', 'POSTTRIAL', 'TRIAL'],
        name: 'Consulation',
        description: 'Test SCI Object',
        seqNumber: 122333,
        rate: 2000,
        unitOfMeasurement: {
          '@class': 'org.legalq.legal.UnitOfMeasurement',
          name: 'Hourly',
          description: 'Measurement value for Consultation hourly rate',
          code: 'B300',
          active: 2,
        },
        canHaveDisbursement: true,
        canEmployCounsel: false,
        active: 1,
        matterTypeEnum: null,
        disbFlag: false,
        matterPhaseInt: 1,
        quantity: 1
      }
     ], // normal sci, roles will be lost
    quoteTotal: 1200,
    quoteDisbursment: 1500
  }


  constructor() { }

  ngOnInit() {
  }

}
