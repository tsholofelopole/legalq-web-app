import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quote-summary-scis',
  templateUrl: './quote-summary-scis.component.html',
  styleUrls: ['./quote-summary-scis.component.css']
})
export class QuoteSummaryScisComponent implements OnInit {

  @Input() sci;

  constructor() { }

  ngOnInit() {
  }

}
