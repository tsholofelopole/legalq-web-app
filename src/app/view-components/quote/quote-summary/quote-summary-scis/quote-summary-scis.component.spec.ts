import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteSummaryScisComponent } from './quote-summary-scis.component';

describe('QuoteSummaryScisComponent', () => {
  let component: QuoteSummaryScisComponent;
  let fixture: ComponentFixture<QuoteSummaryScisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteSummaryScisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteSummaryScisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
