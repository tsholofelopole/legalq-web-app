import { StandardCostItem } from 'src/app/models/standard-cost-item.model';

export class Quotesummary {

  public scis: StandardCostItem[] = []; // normal sci, roles will be lost
  public quoteTotal: number;
  public quoteDisbursment: number;

}
