import { Component, OnInit } from '@angular/core';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { Router } from '@angular/router';
import { UserStoryLineManagerService } from 'src/app/services/view-services/user-story-line-manager.service';


// such override allows to keep some initial values
export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {


  // mock values for quote total
  totalQuote = 1500;
  totalDisbursement = 1200;

  oneAtATime = true;
  constructor(private router: Router,
              private userStoryLineManagerService: UserStoryLineManagerService) { }

  ngOnInit() {
    this.userStoryLineManagerService.currentStoryLine.subscribe(storyline => {
      console.log('\n\n*****Story line: ', storyline, '****\n\n');
    });
  }

  gotoquotesummary() {
    this.router.navigate(['/quotesummary']);
  }

}
