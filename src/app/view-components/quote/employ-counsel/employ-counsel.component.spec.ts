import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployCounselComponent } from './employ-counsel.component';

describe('EmployCounselComponent', () => {
  let component: EmployCounselComponent;
  let fixture: ComponentFixture<EmployCounselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployCounselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployCounselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
