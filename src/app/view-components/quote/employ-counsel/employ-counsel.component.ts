import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EmployCounselService } from 'src/app/services/view-services/employ-cousel.service';

@Component({
  selector: 'app-employ-counsel',
  templateUrl: './employ-counsel.component.html',
  styleUrls: ['./employ-counsel.component.css']
})
export class EmployCounselComponent implements OnInit {

  @Output() fees: EventEmitter<number> = new EventEmitter();
  theFees = 0;
  constructor(private employCounselService: EmployCounselService) { }

  counselSci = [
    {
      description: 'Counsel consulation',
      quantity: 3,
      uom: 'hourly',
      rate: 3000,
      disbursement: 300
    },
    {
      description: 'Counsel consulation',
      quantity: 3,
      uom: 'hourly',
      rate: 3000,
      disbursement: 300
    },
    {
      description: 'Counsel consulation',
      quantity: 3,
      uom: 'hourly',
      rate: 3000,
      disbursement: 300
    }
  ];


  onUpdateFees(fee) {
    this.theFees += fee;
    this.fees.emit(fee);

  }
  // onFeesSet(randomNumber: number) {
  //   this.fees = randomNumber;
  // }

  ngOnInit() {

  }

}
