import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-historycontent',
  templateUrl: './historycontent.component.html',
  styleUrls: ['./historycontent.component.css']
})
export class HistorycontentComponent implements OnInit {

  @Input() historyObject;

  constructor() { }

  ngOnInit() {
    console.log('historyObject ', this.historyObject);
  }

  previewQuoteHistory(historyRef) {
    // call service to get full quote information
    // alert('ref: ' + historyRef);
  }

}
