import { NgModule } from '@angular/core';
import { HistorycontentComponent } from './historycontent.component';

@NgModule({
  declarations: [HistorycontentComponent],
  imports: [
  ],
  exports: [HistorycontentComponent]
})
export class HistorycontentModule { }
