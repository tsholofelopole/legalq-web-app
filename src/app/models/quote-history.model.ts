import { Firm } from './firm.model';
import { Client } from './client.model';
import { DocumentStatus } from './document-status.mode';
import { MatterType } from './matter-type.model';
import { SystemUser } from './system-user.model';

export class QuoteHistory {
    constructor() {
      this['@class'] = 'org.legalq.legal.QuoteHistory';
    }

    public quoteReference: string;
    public firmLegalReference: string;
    public documentStatus: string;
    public statusDate: Date;
    public clientName: string;

    // TODO: remove client from model, use string for user/client instead
    // Create facade to handle inserting new roles, rates and permissions

}
