
export class Permission {
    constructor() {
      this['@class'] = 'org.legalq.legal.Permission';
    }

    public isAdmin: boolean;
    public overWriteDefaultUOM: boolean;
    public addAdditionalEmail: boolean;
    public printQuote: boolean;
    public reuseHistoryQuote: boolean;
}
