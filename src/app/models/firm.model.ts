import { Client } from './client.model';
import { License } from './license.model';
import { Role } from './role.model';
import { QuoteHistory } from './quote-history.model';
import { Clause } from './clause.model';
import { SystemUser } from './system-user.model';

export class Firm {
    constructor() {
      this['@class'] = 'org.legalq.legal.Firm';
    }

    public legalName: string;
    public legalReference: string;
    public address: string;
    public quoteReferencesSeq: number;
    public clients: Client[] = [];
    public licenses: License[] = [];
    public roles: Role[] = [];
    public clauses: Clause[] = [];
    public headerImage: string;
    public footerImage: string; //should be byte[]

}
