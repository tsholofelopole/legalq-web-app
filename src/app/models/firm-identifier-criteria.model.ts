import { Criteria } from './criteria.model';

export class FirmIdentifierCriteria extends Criteria {

  constructor() {
    super();
    this['@class'] = 'org.legalq.legal.FirmIdentifierCriteria';
  }

  public firmLegalReference: string;
}
