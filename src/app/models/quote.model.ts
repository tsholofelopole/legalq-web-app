import { Clause } from './clause.model';
import { Firm } from './firm.model';
import { StandardCostItem } from './standard-cost-item.model';

export class Quote {
  constructor() {
    this['@class'] = 'org.legalq.legal.Quote';
  }

    public quoteIdentifier: string; // unique generated identifier for each quote
    public name: string;
    public description: string;
    public generateBy: string;
    public standardCostItems: StandardCostItem[] = [];
    public quoteClauses: Clause[] = [];
    public firmLegalReference: string;
    public firm: Firm; // quote should always be linked to a firm
}
