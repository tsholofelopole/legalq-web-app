import { Role } from '../role.model';
import { Firm } from '../firm.model';
import { SystemUser } from '../system-user.model';

export class AuthenticationAdvice {
   constructor() {
     this['@class'] = 'org.legalq.legal.AuthenticationAdvice';
   }

    public authenticated: boolean;
    public systemUser: SystemUser;
    public role: Role;
    public firm: Firm;
}
