export class UserNameAndPassword {

  constructor() {
    this['@class'] = 'org.legalq.legal.authentication.UserNameAndPassword';
  }

  public username: string;
  public password: string;
}
