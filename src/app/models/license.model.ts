import { LicenseType } from './license-type.model';

export class License {

  constructor() {
    this['@class'] = 'org.legalq.legal.License';
  }
    public noOfUsers: number;
    public expiryDate: Date;
    public active: number;
    public licenseType: LicenseType;
}
