export enum MatterPhase {
  TRIAL,
  PRETRIAL,
  POSTTRIAL
}
