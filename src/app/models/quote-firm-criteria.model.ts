import { Criteria } from './criteria.model';
import { Firm } from './firm.model';

export class QuoteFirmCriteria extends Criteria {
    // find all the quotes linked to a firm and generate a history
  constructor() {
    super();
    this['@class'] = 'org.legalq.legal.QuoteFirmCriteria';
  }

  public firm: Firm;
}
