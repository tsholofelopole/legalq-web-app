export enum ClauseState {
  ACTIVE,
  CANCELLED,
  DORMANT,
  PAUSED
}
