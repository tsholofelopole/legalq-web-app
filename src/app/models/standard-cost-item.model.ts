import { MatterPhase } from './matter-phase.model';
import { MatterTypeEnum } from './matter-type-enum.model';
import { UnitOfMeasurement } from './unit-of-measurement.model';
import { FirmRate } from './firm-rate.model';

export class StandardCostItem {

    constructor() {
      this['@class'] = 'org.legalq.legal.StandardCostItem';
    }

    public name: string;
    public description: string;
    public seqNumber: number;
    public canEmployCounsel: boolean;
    public canHaveDisbursement: boolean;
    public matterPhases: string[] = [];
    public matterTypes: string[] = [];
    public motionAction: string[] = [];
    public rate: FirmRate;

}
