
export class UnitOfMeasurement {

    constructor() {
      this['@class'] = 'org.legalq.legal.UnitOfMeasurement';
    }

    public name: string;
    public description: string;
    public code: string;
    public active: string;
}
