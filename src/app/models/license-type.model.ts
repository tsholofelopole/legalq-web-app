
export class LicenseType {

    constructor() {
      this['@class'] = 'org.legalq.legal.LicenseType';
    }

    public name: string;
    public status: string;
}
