
export class Client {

    constructor() {
      this['@class'] = 'org.legalq.legal.Client';
    }

    public clientName: string;
    public clientEmail: string;
}
