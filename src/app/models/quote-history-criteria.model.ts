import { Criteria } from './criteria.model';

export class QuoteHistoryCriteria extends Criteria {
   constructor() {
     super();
     this['@class'] = 'org.legalq.legal.QuoteHistoryCriteria';
   }

    public firmLegalReference: string;
}
