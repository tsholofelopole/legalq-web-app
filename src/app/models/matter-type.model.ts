
export class MatterType {

    constructor() {
      this['@class'] = 'org.legalq.legal.MatterType';
    }

    public category: string;
    public type: string;
    public subject: string;
}
