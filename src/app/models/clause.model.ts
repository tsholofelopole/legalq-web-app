import { ClauseState } from './clause-state.model';

export class Clause {

  constructor() {
    this['@class'] = 'org.legalq.legal.Clause';
  }

  public name: string;
  public clause: string;
  public clauseState: string;
}
