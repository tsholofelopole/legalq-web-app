
export class Settings {
    constructor() {
      this['@class'] = 'org.legalq.legal.Settings';
    }

    public VAT: number;
    public companyName: string;
    public companyRegistration: string;
    public companyAddress: string;
    public companyAddressCity: string;
    public companyAddressCode: string;
    public companyVATNumber: string;
    public companyTAXNumber: string;
    public companyContactNumber: string;
    public companyBank: string;
    public companyBankAccNumber: string;
    public companyBranchCode: string;
    public pdfFooter: string;
    public emailBodyText: string;
}
