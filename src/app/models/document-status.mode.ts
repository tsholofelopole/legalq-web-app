export enum DocumentStatus {
  ACTIVE,
  DRAFT,
  ARCHIVED,
  DELETED,
  DISABLED,
  SEALED
}
