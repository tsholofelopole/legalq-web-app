import { Criteria } from './criteria.model';
import { MatterType } from './matter-type.model';
import { MatterPhase } from './matter-phase.model';

export class SCICriteria extends Criteria {

    constructor() {
      super();
      this['@class'] = 'org.legalq.legal.SCICriteria';
    }

    public matterType: string;
    public matterPhase: string;
}
