import { Permission } from './permission.model';

export class Role {
    constructor() {
      this['@class'] = 'org.legalq.legal.Role';
    }
    public name: string;
    public counsel: boolean;
    //change permissions to strings
    // public permissions: string[] = [];
    public permission: Permission;
    // TODO: update/fix
    public status: string;
    public hourlyRate: number;
    public kmRate: number;
    public fifteenMinIntervalRate: number;
}
