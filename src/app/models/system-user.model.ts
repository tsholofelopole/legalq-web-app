
export class SystemUser {

  constructor() {
    this['@class'] = 'org.legalq.legal.SystemUser';
  }

    public userName: string;
    public password: string;
    public UserAlias: string;
    public fullName: string;
    public surname: string;
    public firmLegalReference: string;
    public mobileNumber: string;
    public emailAddress: string;
    public additionalEmailAddress: string;
    public telNumber: string;
    public loggedIn: boolean;
}
