import { UnitOfMeasurement } from './unit-of-measurement.model';

export class FirmRate {

  constructor() {
    this['@class'] = 'org.legalq.legal.FirmRate';
  }
    public name: string;
    public firmLegalRef: string; // changed from primarykeys
    public unitOfMeasurement: UnitOfMeasurement; // 1...* or just 1
    public rate: number;
    public startDate: Date;
    public endDate: Date;
    // public ratesHistory: FirmRate[] = [];
    public active: number;
}
