import { Criteria } from './criteria.model';


export class QuoteIdentifierCriteria extends Criteria {
    constructor() {
      super();
      this['@class'] = 'org.legalq.legal.QuoteIdentifierCriteria';
    }

    public identifier: string;
}
