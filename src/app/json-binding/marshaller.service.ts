// Marshaller with underscores
import { Injectable } from '@angular/core';

@Injectable()
export class MarshallerService {

    marshallObjectToJSON(object) {
        return recursiveMarshaller(object); // parse stringified json to JSON object
    }
}

const classesToBeBound = new Object();

function addClassesToBeBound(clazz) {
    const obj = new clazz();
    classesToBeBound[obj['@class']] = obj;
}
const atIdMarshallingMap = new Map();

function recursiveMarshaller(object) {
    // handle collections
    if (Array.isArray(object)) {
        for (let index = 0; index < object.length; ++index) {
            if (typeof object[index] == 'object') {
                object[index] = recursiveMarshaller(object[index]);
            }
        }
        return object;
    }
    if (object != null && object != undefined) {
        const keys = Object.keys(object);
        const json = new Object();
        atIdMarshallingMap.clear();
// tslint:disable-next-line: forin
        for (const i in keys) {
            const newKey = keys[i];
            // Hanlde objects
            if (typeof object[keys[i]] == 'object') {
                // console.log("\nIts an Object!!! ", object[keys[i]])
                object[keys[i]] = recursiveMarshaller(object[keys[i]]);
            }
            // console.log("\nCurrent Object[keys[i]]: ", object[keys[i]]);
            if (object[keys[i]] == undefined || object[keys[i]] == null) {
                object[keys[i]] = null;
            }
            json[newKey] = object[keys[i]];
        }
        return marshall(json);
    }
}
// Marshall object to JSON
function marshall(object) {
    return JSON.parse(JSON.stringify(object, atIdReplacer));
}

function atIdReplacer(name, val) {
// tslint:disable-next-line: triple-equals
    if (val == undefined || val == null) {
        return val;
    }
// tslint:disable-next-line: triple-equals
    if (val['@Id'] != undefined && val['@Id'] != null) {
        if (atIdMarshallingMap.get(val) != null) {
            return atIdMarshallingMap.get(val);
        } else {
            const id = guid();
            atIdMarshallingMap.set(val, id);
            val['@Id'] = id;
            return val;
        }
    } else {
        return val;
    }
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
