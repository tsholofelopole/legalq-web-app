
import { Injectable } from '@angular/core';
import { ClauseState } from '../models/clause-state.model';
import { Clause } from '../models/clause.model';
import { Client } from '../models/client.model';
import { Criteria } from '../models/criteria.model';
import { DocumentStatus } from '../models/document-status.mode';
import { Firm } from '../models/firm.model';
import { LicenseType } from '../models/license-type.model';
import { License } from '../models/license.model';
import { MatterPhase } from '../models/matter-phase.model';
import { MatterType } from '../models/matter-type.model';
import { Permission } from '../models/permission.model';
import { Quote } from '../models/quote.model';
import { QuoteFirmCriteria } from '../models/quote-firm-criteria.model';
import { QuoteIdentifierCriteria } from '../models/quote-identifier-criteria.model';
import { QuoteHistory } from '../models/quote-history.model';
import { QuoteHistoryCriteria } from '../models/quote-history-criteria.model';
import { Request } from '../models/request.model';
import { Response } from '../models/response.model';
import { Role } from '../models/role.model';
import { SCICriteria } from '../models/sci-criteria.model';
import { Settings } from '../models/settings.model';
import { StandardCostItem } from '../models/standard-cost-item.model';
import { UnitOfMeasurement } from '../models/unit-of-measurement.model';
import { AuthenticationAdvice } from '../models/authentication/authentication-advice.model';
import { UserNameAndPassword } from '../models/authentication/username-and-password.model';
import { AuthenticateUserRequest } from '../services/legal/authenticator/models/authenticate-user-request.model';
import { AuthenticateUserResponse } from '../services/legal/authenticator/models/authenticate-user-reponse.model';
import { FindQuoteRequest } from '../services/legal/quote-persistence/models/find-quote-request.model';
import { FindQuoteResponse } from '../services/legal/quote-persistence/models/find-quote-response.model';
import { InsertQuoteRequest } from '../services/legal/quote-persistence/models/insert-quote-request.model';
import { InsertQuoteResponse } from '../services/legal/quote-persistence/models/insert-quote-reponse.model';
import { RequestNotValidException } from '../services/helper-services/exception-handling/request-not-valid-exception.model';
import { Exception } from '../services/helper-services/exception-handling/exception.model';
import { UnexpectedServiceError } from '../services/helper-services/exception-handling/unexpected-service-error.model';
import { FirmRate } from '../models/firm-rate.model';
import { SystemUser } from '../models/system-user.model';
import { FindFirmRateRequest } from '../services/legal/firm-rate-persistence/models/find-firm-rate-request.model';
import { FindFirmRateResponse } from '../services/legal/firm-rate-persistence/models/find-firm-rate-response.model';
import { InsertFirmRateRequest } from '../services/legal/firm-rate-persistence/models/insert-firm-rate-request.model';
import { InsertQuoteHistoryRequest } from '../services/legal/quote-history-persistence/models/insert-quote-history-request.model';
import { UpdateFirmRateRequest } from '../services/legal/firm-rate-persistence/models/update-firm-rate-request.model';
import { UpdateFirmRateResponse } from '../services/legal/firm-rate-persistence/models/update-firm-rate-response.model';
import { FindStandardCostItemRequest } from '../services/legal/standard-cost-item-persistence/models/find-standard-cost-item-request.model';
import { UpdateStandardCostItemRequest } from '../services/legal/standard-cost-item-persistence/models/update-standard-cost-item-request.model';
import { FindQuoteHistoryResponse } from '../services/legal/quote-history-persistence/models/find-quote-history-response.model';
import { FindFirmRatesResponse } from '../services/legal/firm-rate-persistence/models/find-firm-reates-response.models';
import { FindFirmRatesRequest } from 'src/app/services/legal/firm-rate-persistence/models/find-firm-rates-request.model';
@Injectable()
export class UnMarshallerService {

    constructor() {
        // project models
        //addClassesToBeBound(ClauseState);
        addClassesToBeBound(Clause);
        addClassesToBeBound(Client);
        addClassesToBeBound(Criteria);
        //addClassesToBeBound(DocumentStatus);
        addClassesToBeBound(Firm);
        addClassesToBeBound(LicenseType);
        addClassesToBeBound(License);
        //addClassesToBeBound(MatterPhase);
        addClassesToBeBound(MatterType);
        addClassesToBeBound(Permission);
        addClassesToBeBound(Quote);
        addClassesToBeBound(QuoteFirmCriteria);
        addClassesToBeBound(QuoteIdentifierCriteria);
        addClassesToBeBound(QuoteHistory);
        addClassesToBeBound(QuoteHistoryCriteria);
        addClassesToBeBound(FirmRate);
        addClassesToBeBound(Request);
        addClassesToBeBound(Response);
        addClassesToBeBound(Role);
        addClassesToBeBound(SCICriteria);
        addClassesToBeBound(Settings);
        addClassesToBeBound(StandardCostItem);
        addClassesToBeBound(UnitOfMeasurement);
        addClassesToBeBound(SystemUser);
        addClassesToBeBound(AuthenticationAdvice);
        addClassesToBeBound(UserNameAndPassword);
        addClassesToBeBound(AuthenticateUserRequest);
        addClassesToBeBound(AuthenticateUserResponse);
        addClassesToBeBound(FindQuoteRequest);
        addClassesToBeBound(FindQuoteResponse);
        addClassesToBeBound(InsertQuoteRequest);
        addClassesToBeBound(InsertQuoteResponse);
        addClassesToBeBound(RequestNotValidException);
        addClassesToBeBound(Exception);
        addClassesToBeBound(UnexpectedServiceError);

        addClassesToBeBound(FindFirmRateRequest);
        addClassesToBeBound(FindFirmRateResponse);
        addClassesToBeBound(InsertFirmRateRequest);
        addClassesToBeBound(InsertQuoteHistoryRequest);
        addClassesToBeBound(UpdateFirmRateRequest);
        addClassesToBeBound(UpdateFirmRateResponse);
        addClassesToBeBound(FindQuoteHistoryResponse);
        addClassesToBeBound(FindFirmRatesResponse);
        addClassesToBeBound(FindFirmRatesRequest);
        // addClassesToBeBound(RequestNotValidException);
        // addClassesToBeBound(Exception);
        // addClassesToBeBound(UnexpectedServiceError);
        // addClassesToBeBound(UpdateWimSteeringOverloadFactorRequest);
        // addClassesToBeBound(UpdateWimMaxSpeedResponse);
        // addClassesToBeBound(ConceptualDocument);

        // addClassesToBeBound(RenderDocumentResponse);
    }

    unMarshallFromJson(Object, clazz) {
        return unmarshall(Object, clazz); // takes a json string, call marshaller before demarshalling
    }
}

function addClassesToBeBound(clazz) {
  // console.log("Clazz; ", clazz);
  var obj = new clazz.prototype.constructor();
  if (obj['@class'] == undefined) {
      classes[clazz.prototype.constructor.name] = clazz;
  }
  else {
      classes[obj['@class']] = clazz;
  }
}

var classes = new Array();
var atIdUnMarshallingMap = new Map();

//Unmarshall from JSON to object
function unmarshall(jsonObject, clazz) {
  atIdUnMarshallingMap.clear();
  return unmarshallRecursive(jsonObject, clazz);
}

function unmarshallRecursive(jsonObject, clazz) {
  // console.log("jsonObject", jsonObject);
  //If primitive, return jsonObject
  if (typeof jsonObject != "object") {
      if (atIdUnMarshallingMap.get(jsonObject) != null && atIdUnMarshallingMap.get(jsonObject) != undefined) {
          return atIdUnMarshallingMap.get(jsonObject);
      }
      else {
          return jsonObject;
      }
  }
  //handle collections
  if (Array.isArray(jsonObject)) {
      for (var index = 0; index < jsonObject.length; ++index) {
          jsonObject[index] = unmarshallRecursive(jsonObject[index], undefined);
      }
      return jsonObject;
  }

  var keys = undefined;

  var theClass = undefined;
  if (jsonObject == null || jsonObject == undefined) {
      jsonObject = null;
  }
  else {
      //get object keys
      // console.log("Getting keys in the object", keys);
      keys = Object.keys(jsonObject);
      theClass = jsonObject['@class'];
  }
  if (theClass != undefined) {
      clazz = classes[theClass];
  }
  else if (theClass == undefined) {
      // console.log("Object value is null");
      // throw new RequestNotValidException();
  }

  var clazzObject;
  if (clazz != undefined && clazz != null) {
      clazzObject = new clazz.prototype.constructor();
  }
  else {
      clazzObject = new Object();
  }
  // var objectKeys = Object.keys(clazzObject);
// tslint:disable-next-line: forin
  for (var i in keys) {
      clazzObject[keys[i]] = unmarshallRecursive(jsonObject[keys[i]], undefined);
  }
  if (clazzObject['@Id'] != undefined && clazzObject['@Id'] != null) {
      var id = clazzObject['@Id'];
      atIdUnMarshallingMap.set(id, clazzObject);
  }
  return clazzObject;
}
