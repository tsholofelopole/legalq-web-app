import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UnMarshallerService } from './unmarshaller.service';
import { MarshallerService } from './marshaller.service';

@NgModule({
  declarations: [],
  imports: [
    RouterModule
  ],
  exports: [],
  providers: [
    UnMarshallerService,
    MarshallerService
//provide marshaller and unmarshallerservice

  ]
})
export class JsonBindingModule { }
