import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class DisplayQuoteManager {

  displayQuote  = new BehaviorSubject<boolean>(false);
  currentdisplayQuote = this.displayQuote.asObservable();

  displayQuoteStateUpdate(state: boolean) {
    this.displayQuote.next(state);
  }

}
