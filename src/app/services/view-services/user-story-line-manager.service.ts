import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserStoryLineManagerService {

  storyLine  = new BehaviorSubject<string>('');
  currentStoryLine = this.storyLine.asObservable();

  updateStoryLine(state: string) {

    // this.currentStoryLine.subscribe(st => {
      const story = (this.storyLine.getValue() + ', ' + state).toUpperCase();
      this.storyLine.next(story);
    // });

  }

}
