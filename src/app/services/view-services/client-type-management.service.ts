import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ClientTypeManagement {

  clientType  = new BehaviorSubject<string>('');
  currentClientType = this.clientType.asObservable();

  updateClientType(state: string) {
    this.clientType.next(state);
  }

}
