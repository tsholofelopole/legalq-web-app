import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MotionTypeManagerService {

  motionType = new BehaviorSubject<string>('');
  currentMotionType = this.motionType.asObservable();

  setMotionType(motion: string) {
    this.motionType.next(motion);
  }


}
