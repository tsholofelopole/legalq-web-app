import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DocumentTemplateManagerService {

  documentHeaderUrl = new BehaviorSubject<string>('');
  currentDocHeaderValue = this.documentHeaderUrl.asObservable();

  footerImageUri = new BehaviorSubject<string>('');
  currentfooterImageUri = this.footerImageUri.asObservable();

  setDocumentHeaderTemplateValue(doc: string) {
    this.documentHeaderUrl.next(doc);
  }

  updateFooterImage(footer: string) {
    this.footerImageUri.next(footer);
  }

}
