import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CustomLoaderService {

  loaderState  = new BehaviorSubject<boolean>(false);
  currentState = this.loaderState.asObservable();

  updateLoaderState(state: boolean) {
    this.loaderState.next(state);
  }

}
