import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class EmployCounselService {

    employCounsel  = new BehaviorSubject<boolean>(false);
    employCounselState = this.employCounsel.asObservable();

    updateEmployCounselValue(state: boolean) {
      this.employCounsel.next(state);
    }
}
