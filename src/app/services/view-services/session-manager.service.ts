import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SessionManagerService {

  endSession  = new BehaviorSubject<boolean>(false);
  currentState = this.endSession.asObservable();

  displaySessionManagerMessage(state: boolean) {
    this.endSession.next(state);
  }
}
