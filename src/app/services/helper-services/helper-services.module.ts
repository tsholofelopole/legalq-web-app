import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExceptionParserService } from './exception-handling/exception-parser.service';
import { FirmInSessionService } from './firm-in-session.service';

@NgModule({
  declarations: [],
  imports: [ CommonModule ],
  exports: [],
  providers: [ExceptionParserService,
    FirmInSessionService],
})
export class HelperServicesModule {}
