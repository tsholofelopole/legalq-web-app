import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FirmInSessionService {

  firmLegalReference  = new BehaviorSubject<string>('TEST');
  currentFirmLegalReference = this.firmLegalReference.asObservable();

  setFirmLegalReference(firm: string) {
    this.firmLegalReference.next(firm);
  }
}
