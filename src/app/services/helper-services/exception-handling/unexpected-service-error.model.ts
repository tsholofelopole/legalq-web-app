import { Exception } from './exception.model';

export class UnexpectedServiceError extends Exception {

  constructor() {
    super();
    this['@class'] = 'org.legalq.services.UnexpectedServiceError';
  }
}
