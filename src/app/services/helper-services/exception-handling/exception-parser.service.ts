
import { Injectable } from "@angular/core";
import { UnexpectedServiceError } from './unexpected-service-error.model';
import { RequestNotValidException } from './request-not-valid-exception.model';

@Injectable()
export class ExceptionParserService {

    constructor() { }

    parse(exception, body) {
      switch (exception) {
        case 'org.legalq.services.UnexpectedServiceError':
          return new UnexpectedServiceError();
        case 'org.legalq.services.RequestNotValidException':
          return new RequestNotValidException();
        case 'java.awt.print.PrinterAbortException':
          return new UnexpectedServiceError();
        default:
          return null;
        }
    }
}

