import { Exception } from './exception.model';

export class RequestNotValidException extends Exception {

  constructor() {
    super();
    this['@class'] = 'org.legalq.services.RequestNotValidException';
  }
}
