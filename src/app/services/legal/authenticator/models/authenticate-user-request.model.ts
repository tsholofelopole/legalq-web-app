import { UserNameAndPassword } from '../../../../models/authentication/username-and-password.model';
import { Request } from '../../../../models/request.model';
export class AuthenticateUserRequest extends Request {

  constructor() {
    super();
  }
  public userNameAndPassword: UserNameAndPassword;
}
