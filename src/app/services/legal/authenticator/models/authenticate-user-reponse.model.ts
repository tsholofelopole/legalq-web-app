import { Response } from '../../../../models/response.model';
import { AuthenticationAdvice } from 'src/app/models/authentication/authentication-advice.model';

export class AuthenticateUserResponse extends Response {

  constructor() {
    super();
  }
  public  authenticationAdvice: AuthenticationAdvice;
}
