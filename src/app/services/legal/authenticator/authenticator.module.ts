import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthenticateUserManager } from './authenicate-user/authenticate-user-manager.service';
import { HttpClientModule } from '@angular/common/http';
import { JsonBindingModule } from 'src/app/json-binding/json-binding.module';
import { AuthenticateUser } from './authenicate-user/http/authenticate-user.service';
import { HelperServicesModule } from '../../helper-services/helper-services.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    JsonBindingModule
  ],
  exports: [],
  providers: [
    AuthenticateUserManager,
    AuthenticateUser,
    HelperServicesModule

  ]
})
export class AuthenticatorModule { }
