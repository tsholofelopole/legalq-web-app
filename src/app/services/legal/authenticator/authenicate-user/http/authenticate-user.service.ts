import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthenticateUserRequest } from '../../models/authenticate-user-request.model';
import { MarshallerService } from 'src/app/json-binding/marshaller.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class AuthenticateUser {

  constructor(private http: HttpClient,
              private marshallerService: MarshallerService )  {}

  authenticUri = 'http://' + environment.host + ':' + environment.port + '/legalq/api/org/legal/authenticator/authenticateuser';

  private headers = new HttpHeaders ({
    'Content-Type': 'application/json',
      'Accept-Encoding': 'application/gzip',
      'Access-Control-Allow-Origin': '*',
  });

  authenticateUser(authenticateUserRequest: AuthenticateUserRequest) {

    authenticateUserRequest = this.marshallerService.marshallObjectToJSON(authenticateUserRequest); // call to marshaler to json
    console.log('Request obj: ', authenticateUserRequest);
    return this.http.post(this.authenticUri, authenticateUserRequest,  {headers : this.headers});
  }
}
