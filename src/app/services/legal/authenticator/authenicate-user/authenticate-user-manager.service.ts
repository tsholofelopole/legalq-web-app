import { Injectable } from '@angular/core';
import { AuthenticateUser } from './http/authenticate-user.service';
import { BehaviorSubject } from 'rxjs';
import { AuthenticateUserResponse } from '../models/authenticate-user-reponse.model';
import { AuthenticateUserRequest } from '../models/authenticate-user-request.model';
import { UserNameAndPassword } from 'src/app/models/authentication/username-and-password.model';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { Exception } from 'src/app/services/helper-services/exception-handling/exception.model';
import { ExceptionParserService } from 'src/app/services/helper-services/exception-handling/exception-parser.service';

@Injectable()
export class AuthenticateUserManager {

  constructor(private authenticateUserService: AuthenticateUser,
              private unMarshallerService: UnMarshallerService,
              private exceptionparserService: ExceptionParserService) {}

  authenticateUserResponseObservable = new BehaviorSubject<AuthenticateUserResponse>(null);
  authenticateUserException = new BehaviorSubject<Exception>(null);

  authenticateUser(username: string, password: string) {

    const authenticateUserRequest = new AuthenticateUserRequest();
    const userNameAndPassword = new UserNameAndPassword();

    userNameAndPassword.username = username;
    userNameAndPassword.password = password;

    authenticateUserRequest.userNameAndPassword = userNameAndPassword;
    console.log('Username and Password Request: ', authenticateUserRequest);

    this.authenticateUserService.authenticateUser(authenticateUserRequest)
    .subscribe((response: Response): any => {

      console.log("*Auth Response: ", response);
      console.log('HEADERS: ', response.headers);
      const exception = null;
      //const exception = response.headers.get('X-Throwable-Type');

      if (exception) {
        this.authenticateUserException.next(this.exceptionparserService.parse(exception, ''));
      }  else {
        //const data = response.json();

        let authenticateUserResponse = new AuthenticateUserResponse();
        authenticateUserResponse = this.unMarshallerService.unMarshallFromJson(response, AuthenticateUserResponse);
        console.log("\n\n****Auth Response: ", authenticateUserResponse);
        this.authenticateUserResponseObservable.next(authenticateUserResponse);
      }
      });

  }

}
