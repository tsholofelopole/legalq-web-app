import { Response } from 'src/app/models/response.model';

export class InsertStandardCostItemResponse extends Response {
  constructor() {
    super();
  }
}
