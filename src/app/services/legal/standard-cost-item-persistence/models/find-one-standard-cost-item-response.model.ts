import { Response } from 'src/app/models/response.model';
import { StandardCostItem } from 'src/app/models/standard-cost-item.model';

export class FindUniqueStandardCostItemResponse extends Response {
  constructor() {
    super();
  }

  public standardCostItem: StandardCostItem;
}
