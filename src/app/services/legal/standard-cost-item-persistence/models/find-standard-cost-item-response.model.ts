import { Response } from 'src/app/models/response.model';
import { StandardCostItem } from 'src/app/models/standard-cost-item.model';

export class FindStandardCostItemResponse extends Response {

  constructor() {
    super();
  }

  public standardCostItems: StandardCostItem[] =[];
}
