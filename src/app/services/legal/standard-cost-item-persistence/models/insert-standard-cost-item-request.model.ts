import { Request } from 'src/app/models/request.model';
import { StandardCostItem } from 'src/app/models/standard-cost-item.model';

export class InsertStandardCostItemRequest extends Request {
  constructor() {
    super();
  }

  public standardCostItem: StandardCostItem;
}
