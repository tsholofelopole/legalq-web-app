import { Response } from 'src/app/models/response.model';

export class UpdateStandardCostItemResponse extends Response {
  constructor() {
    super();
  }
}
