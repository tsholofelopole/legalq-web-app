import { Request } from 'src/app/models/request.model';

export class FindUniqueStandardCostItemRequest extends Request {
  constructor() {
    super();
  }

  public sciName: string;
}
