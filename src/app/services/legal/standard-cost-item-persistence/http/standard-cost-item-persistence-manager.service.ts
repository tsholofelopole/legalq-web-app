import { Injectable } from '@angular/core';
import { MarshallerService } from 'src/app/json-binding/marshaller.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { InsertStandardCostItemRequest } from '../models/insert-standard-cost-item-request.model';
import { FindStandardCostItemRequest } from '../models/find-standard-cost-item-request.model';
import { UpdateStandardCostItemRequest } from '../models/update-standard-cost-item-request.model';
import { FindUniqueStandardCostItemRequest } from '../models/find-one-standard-cost-item-request.model';

@Injectable()
export class StandardCostItemPersistenceManager {

    constructor(private http: HttpClient,
                private marshallerService: MarshallerService)  {}

    sciUri = 'http://' + environment.host + ':' + environment.port + '/legalq/api/org/legal/standardcostitempersistencemanager';

    private headers = new HttpHeaders ({
    'Content-Type': 'application/json'
    });

    insertSCI(insertStandardCostItemRequest: InsertStandardCostItemRequest) {

      const insertSciUri = this.sciUri + '/insertsci';
      insertStandardCostItemRequest = this.marshallerService.marshallObjectToJSON(insertStandardCostItemRequest);

      return this.http.post(insertSciUri, insertStandardCostItemRequest,  {headers : this.headers});
    }

    findSCI(findStandardCostItemRequest: FindStandardCostItemRequest) {

      const findSciUri = this.sciUri + '/findsci';
      findStandardCostItemRequest = this.marshallerService.marshallObjectToJSON(findStandardCostItemRequest);

      return this.http.post(findSciUri, findStandardCostItemRequest,  {headers : this.headers});

    }

    updateSci(updateStandardCostItemRequest: UpdateStandardCostItemRequest) {
      const updateSciUri = this.sciUri + '/updatesci';
      updateStandardCostItemRequest = this.marshallerService.marshallObjectToJSON(updateStandardCostItemRequest);

      return this.http.post(updateSciUri, updateStandardCostItemRequest, {headers: this.headers});
    }

    findOneSci(findUniqueStandardCostItemRequest: FindUniqueStandardCostItemRequest) {
      const findOneSciUri = this.sciUri + '/findonesci';
      findUniqueStandardCostItemRequest = this.marshallerService.marshallObjectToJSON(findUniqueStandardCostItemRequest);

      return this.http.post(findOneSciUri, findUniqueStandardCostItemRequest, {headers: this.headers});
    }

}
