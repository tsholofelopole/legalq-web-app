import { Injectable } from '@angular/core';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { StandardCostItemPersistenceManager } from './http/standard-cost-item-persistence-manager.service';
import { BehaviorSubject } from 'rxjs';
import { InsertStandardCostItemResponse } from './models/insert-standard-cost-item-response.model';
import { FindStandardCostItemResponse } from './models/find-standard-cost-item-response.model';
import { FindStandardCostItemRequest } from './models/find-standard-cost-item-request.model';
import { StandardCostItem } from 'src/app/models/standard-cost-item.model';
import { InsertStandardCostItemRequest } from './models/insert-standard-cost-item-request.model';
import { UpdateStandardCostItemResponse } from './models/update-standard-cost-item-response.model';
import { Exception } from '../../helper-services/exception-handling/exception.model';
import { UpdateStandardCostItemRequest } from './models/update-standard-cost-item-request.model';
import { ExceptionParserService } from '../../helper-services/exception-handling/exception-parser.service';
import { FindUniqueStandardCostItemRequest } from './models/find-one-standard-cost-item-request.model';
import { FindUniqueStandardCostItemResponse } from './models/find-one-standard-cost-item-response.model';

@Injectable()
export class StandardCostItemPersistenceManagement {

  constructor(private unMarshallerService: UnMarshallerService,
              private exceptionparserService: ExceptionParserService,
              private standardCostItemPersistenceManager: StandardCostItemPersistenceManager) {}

    // observables
    insertStandardCostItemResponseObservable = new BehaviorSubject<InsertStandardCostItemResponse>(null);
    findStandardCostItemResponseObservable = new BehaviorSubject<FindStandardCostItemResponse>(null);
    updateStandardCostItemResponseObservable = new BehaviorSubject<UpdateStandardCostItemResponse>(null);
    findUniqueStandardCostItemResponseObservable = new BehaviorSubject<FindUniqueStandardCostItemResponse>(null);

    standardCostItemExceptionObservable = new BehaviorSubject<Exception>(null);

    findSci() {

      const findStandardCostItemRequest = new FindStandardCostItemRequest();
      // set standard cost item to something, send as a req parameter
      this.standardCostItemPersistenceManager.findSCI(findStandardCostItemRequest).subscribe((response: Response): any => {

        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
          this.standardCostItemExceptionObservable.next(this.exceptionparserService.parse(exception, ''));
        } else {
          // const data = response.json();
          let findStandardCostItemResponse = new FindStandardCostItemResponse();
          findStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(response, FindStandardCostItemResponse);
          // findStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(data, FindStandardCostItemResponse);
          console.log('Response: ', findStandardCostItemResponse);
          this.findStandardCostItemResponseObservable.next(findStandardCostItemResponse);
        }
      });
    }

    insertSci(standardCostItem: StandardCostItem) {

      const insertStandardCostItemRequest = new InsertStandardCostItemRequest();
      // set sci req sci

      insertStandardCostItemRequest.standardCostItem = standardCostItem;

      this.standardCostItemPersistenceManager.insertSCI(insertStandardCostItemRequest).subscribe((response: Response): any => {
        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
          this.standardCostItemExceptionObservable.next(this.exceptionparserService.parse(exception, ''));
        } else {
          // const data = response.json();
          let insertStandardCostItemResponse = new InsertStandardCostItemResponse();
          insertStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(response, InsertStandardCostItemResponse);
          // insertStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(data, InsertStandardCostItemResponse);
          console.log('Response: ', insertStandardCostItemResponse);
          this.insertStandardCostItemResponseObservable.next(insertStandardCostItemResponse);
        }
      });
    }

    updateSCI(standardCostItem: StandardCostItem) {

      const updateStandardCostItemRequest = new UpdateStandardCostItemRequest();
      updateStandardCostItemRequest.standardCostItem = standardCostItem;

      this.standardCostItemPersistenceManager.updateSci(updateStandardCostItemRequest).subscribe(
        (response: Response): any => {
          const exception = null;
          // const exception = response.headers.get('X-Throwable-Type');

          if (exception) {
            this.standardCostItemExceptionObservable.next(this.exceptionparserService.parse(exception, ''));
          } else {

            // const data = response.json();
            let updateStandardCostItemResponse = new UpdateStandardCostItemResponse();
            updateStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(response, UpdateStandardCostItemResponse);
            // updateStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(data, UpdateStandardCostItemResponse);
            console.log('Update Response');
            this.updateStandardCostItemResponseObservable.next(updateStandardCostItemResponse);
          }
        });
    }

    findOneSci(sciName: string) {

      const findUniqueStandardCostItemRequest = new FindUniqueStandardCostItemRequest();
      findUniqueStandardCostItemRequest.sciName = sciName;

      this.standardCostItemPersistenceManager.findOneSci(findUniqueStandardCostItemRequest).subscribe(
        (response: Response): any => {
          const exception = null;
          // const exception = response.headers.get('X-Throwable-Type');

          if (exception) {
            this.standardCostItemExceptionObservable.next(this.exceptionparserService.parse(exception, ''));
          } else {
            // const data = response.json();
            let findUniqueStandardCostItemResponse = new FindUniqueStandardCostItemResponse();
            findUniqueStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(response, UpdateStandardCostItemResponse);
            // findUniqueStandardCostItemResponse = this.unMarshallerService.unMarshallFromJson(data, UpdateStandardCostItemResponse);
            console.log('find one sci response: ', findUniqueStandardCostItemResponse);
            this.findUniqueStandardCostItemResponseObservable.next(findUniqueStandardCostItemResponse);
          }
        });


    }

}
