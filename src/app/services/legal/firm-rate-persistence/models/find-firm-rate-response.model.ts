import { Response } from 'src/app/models/response.model';
import { FirmRate } from 'src/app/models/firm-rate.model';

export class FindFirmRateResponse extends Response {
  constructor() {
   super();
  }

  public firmRate: FirmRate;
}
