import { Response } from 'src/app/models/response.model';
import { UnitOfMeasurement } from 'src/app/models/unit-of-measurement.model';

export class FindUOMResponse extends Response {
  constructor() {
    super();
  }

  public uom: UnitOfMeasurement;
}
