import { Request } from 'src/app/models/request.model';

export class FindFirmRatesRequest extends Request {
  constructor() {
    super();
  }

  public legalFirmRef: string;
}
