import { Request } from 'src/app/models/request.model';
import { FirmRate } from 'src/app/models/firm-rate.model';

export class UpdateFirmRateRequest extends Request {
  constructor() {
    super();
  }

  public firmRate: FirmRate;
}
