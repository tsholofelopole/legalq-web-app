import { Response } from 'src/app/models/response.model';
import { FirmRate } from './../../../../models/firm-rate.model';

export class FindFirmRatesResponse extends Response {
  [x: string]: FirmRate[];
  constructor() {
    super();
  }

  public firmRate: FirmRate[] = [];
}
