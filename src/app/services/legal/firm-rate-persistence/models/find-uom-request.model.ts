import { Request } from 'src/app/models/request.model';

export class FindUOMRequest extends Request {
  constructor() {
    super();
  }

  public uomName: string;
}
