import { Request } from 'src/app/models/request.model';
import { FirmRate } from 'src/app/models/firm-rate.model';

export class InsertFirmRateRequest extends Request {
  constructor() {
    super();
  }
  public firmRate: FirmRate;
}
