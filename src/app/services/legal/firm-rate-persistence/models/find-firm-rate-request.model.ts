import { Request } from 'src/app/models/request.model';

export class FindFirmRateRequest extends Request {
  constructor() {
    super();
  }

  public firmRateName: string;
}
