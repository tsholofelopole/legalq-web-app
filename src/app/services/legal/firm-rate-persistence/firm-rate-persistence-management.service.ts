import { Injectable } from '@angular/core';
import { FirmRatePersistenceManager } from './http/firm-rate-persistence-manager.service';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { ExceptionParserService } from '../../helper-services/exception-handling/exception-parser.service';
import { FindFirmRateRequest } from './models/find-firm-rate-request.model';
import { UpdateFirmRateRequest } from './models/update-firm-rate-request.model';
import { BehaviorSubject } from 'rxjs';
import { FindFirmRateResponse } from './models/find-firm-rate-response.model';
import { UpdateFirmRateResponse } from './models/update-firm-rate-response.model';
import { Exception } from '../../helper-services/exception-handling/exception.model';
import { InsertFirmRateResponse } from './models/insert-firm-rate-response.model';
import { InsertFirmRateRequest } from './models/insert-firm-rate-request.model';
import { FindFirmRatesResponse } from './models/find-firm-reates-response.models';
import { FindFirmRatesRequest } from './models/find-firm-rates-request.model';
import { HttpResponse } from '@angular/common/http';
import { FindUOMRequest } from './models/find-uom-request.model';
import { FindUOMResponse } from './models/find-uom-response.model';

@Injectable()
export class FirmRatePersistenceManagementService {

  constructor(private unMarshallerService: UnMarshallerService,
              private exceptionparserService: ExceptionParserService,
              private firmRatePersistenceManager: FirmRatePersistenceManager) {}

  findFirmRateResponseObservable = new BehaviorSubject<FindFirmRateResponse>(null);
  updateFirmRateResponseObservable = new BehaviorSubject<UpdateFirmRateResponse>(null);
  insertFirmRateResponseObservable = new BehaviorSubject<InsertFirmRateResponse>(null);
  findFirmRatesResponseObservable = new BehaviorSubject<FindFirmRatesResponse>(null);
  findUomResponseObservable = new BehaviorSubject<FindUOMResponse>(null);
  exception = new BehaviorSubject<Exception>(null);

  insertFirmRate(insertFirmRateRequest: InsertFirmRateRequest) {
    this.firmRatePersistenceManager.insertFirmRate(insertFirmRateRequest).subscribe(
      (response: HttpResponse<InsertFirmRateResponse>): any => {
        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          // const data = response.json();
          let insertFirmRateResponse = new InsertFirmRateResponse();
          insertFirmRateResponse = this.unMarshallerService.unMarshallFromJson(response, InsertFirmRateResponse);
          // insertFirmRateResponse = this.unMarshallerService.unMarshallFromJson(InsertFirmRateResponse, data);
          this.insertFirmRateResponseObservable.next(insertFirmRateResponse);
        }
      }
    );
  }

  findFirmRate(findFirmRateRequest: FindFirmRateRequest) {
    this.firmRatePersistenceManager.findFirmRate(findFirmRateRequest).subscribe(
      (response: HttpResponse<FindFirmRateResponse>): any => {
        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          // const data = response.json();
          let findFirmRateResponse = new FindFirmRateResponse();
          findFirmRateResponse = this.unMarshallerService.unMarshallFromJson(response, FindFirmRateResponse);
          // findFirmRateResponse = this.unMarshallerService.unMarshallFromJson(FindFirmRateResponse, data);
          console.log("\n\nFind fime rate response: ", findFirmRateResponse);
          this.findFirmRateResponseObservable.next(findFirmRateResponse);
        }
      }
    );
  }

  updateFirmRate(updateFirmRateRequest: UpdateFirmRateRequest) {
    this.firmRatePersistenceManager.updateFirmRate(updateFirmRateRequest).subscribe(
      (response: HttpResponse<UpdateFirmRateResponse>): any => {
        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          // const data = response.json();
          let updateFirmRateResponse = new UpdateFirmRateResponse();
          updateFirmRateResponse = this.unMarshallerService.unMarshallFromJson(response, UpdateFirmRateResponse);
          // updateFirmRateResponse = this.unMarshallerService.unMarshallFromJson(UpdateFirmRateResponse, data);
          this.updateFirmRateResponseObservable.next(updateFirmRateResponse);
        }
      }
    );
  }

  findFirmRates(findFirmRatesRequest: FindFirmRatesRequest ) {
    this.firmRatePersistenceManager.findFirmRates(findFirmRatesRequest).subscribe(
      (response: HttpResponse<FindFirmRatesResponse>): any => {
        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          let findFirmRatesResponse = new FindFirmRatesResponse();
          findFirmRatesResponse = this.unMarshallerService.unMarshallFromJson(response, FindFirmRatesResponse);
          // updateFirmRateResponse = this.unMarshallerService.unMarshallFromJson(UpdateFirmRateResponse, data);
          this.findFirmRatesResponseObservable.next(findFirmRatesResponse);
        }
      }
    );
  }

  findUom(findUOMRequest: FindUOMRequest) {
    this.firmRatePersistenceManager.findUom(findUOMRequest).subscribe(
      (response: HttpResponse<FindUOMResponse>): any => {
        const exception = null;
        if (exception) {
          this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          let findUomResponse = new FindUOMResponse();
          findUomResponse = this.unMarshallerService.unMarshallFromJson(response, FindUOMResponse);
          this.findUomResponseObservable.next(findUomResponse);
        }
      }
    );
  }


}
