import { Injectable } from '@angular/core';
import { MarshallerService } from 'src/app/json-binding/marshaller.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { UpdateFirmRateRequest } from '../models/update-firm-rate-request.model';
import { FindFirmRateRequest } from '../models/find-firm-rate-request.model';
import { InsertFirmRateRequest } from '../models/insert-firm-rate-request.model';
import { FindFirmRatesRequest } from '../models/find-firm-rates-request.model';
import { FindUOMRequest } from '../models/find-uom-request.model';


@Injectable()
export class FirmRatePersistenceManager {

  constructor(private http: HttpClient,
              private marshallerService: MarshallerService)  {}

    firmratepersistenceUri = 'http://' + environment.host + ':' + environment.port + '/legalq/api/org/legal/firmratepersistencemanager';

    private headers = new HttpHeaders ({
    'Content-Type': 'application/json'
    });

    insertFirmRate(insertFirmRateRequest: InsertFirmRateRequest) {
      const insertFirmRateURI = this.firmratepersistenceUri + '/insertfirmrate';
      insertFirmRateRequest = this.marshallerService.marshallObjectToJSON(insertFirmRateRequest);
      return this.http.post(insertFirmRateURI, insertFirmRateRequest, {headers: this.headers});
    }

    updateFirmRate(updateFirmRateRequest: UpdateFirmRateRequest) {

      const updateFirmRateUri = this.firmratepersistenceUri + '/updatefirmrate';
      updateFirmRateRequest = this.marshallerService.marshallObjectToJSON(updateFirmRateRequest);
      return this.http.post(updateFirmRateUri, updateFirmRateRequest, {headers: this.headers});
    }

    findFirmRate(findFirmRateRequest: FindFirmRateRequest) {

      const findFirmRateUri = this.firmratepersistenceUri + '/findfirmrate';
      findFirmRateRequest = this.marshallerService.marshallObjectToJSON(findFirmRateRequest);
      console.log("\n\n\n\n\Rate json: ", findFirmRateRequest, "\n\n\n\n\n");
      return this.http.post(findFirmRateUri, findFirmRateRequest, {headers: this.headers});

    }

    findFirmRates(findFirmRatesRequest: FindFirmRatesRequest ) {

      const findFirmRatesUri = this.firmratepersistenceUri + '/findfirmrates';
      findFirmRatesRequest = this.marshallerService.marshallObjectToJSON(findFirmRatesRequest);
      return this.http.post(findFirmRatesUri, findFirmRatesRequest,  {headers: this.headers});
    }

    findUom(findUOMRequest: FindUOMRequest) {
      const findUomUri = this.firmratepersistenceUri + '/finduom';
      findUOMRequest = this.marshallerService.marshallObjectToJSON(findUOMRequest);
      return this.http.post(findUomUri, findUOMRequest, {headers: this.headers});
    }

}
