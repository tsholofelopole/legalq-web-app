import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuotePersistenceManager } from './quote-persistence/http/quote-persistence-manager.service';
import { QuotePersistenceManagerManagement } from './quote-persistence/quote-persistence-manager-management.service';
import { JsonBindingModule } from 'src/app/json-binding/json-binding.module';
import { HttpClientModule } from '@angular/common/http';
import { StandardCostItemPersistenceManager } from './standard-cost-item-persistence/http/standard-cost-item-persistence-manager.service';
import { StandardCostItemPersistenceManagement } from './standard-cost-item-persistence/standard-cost-item-persistence-management.service';
import { QuoteHistoryPersistenceManager } from './quote-history-persistence/http/quote-history-persistence-manager.service';
import { FirmPersistenceManager } from './firm-persistence/http/firm-persistence-manager.service';
import { FirmPersistenceManagement } from './firm-persistence/firm-persistence-management.service';
import { QuoteHistoryPersistenceManagement } from './quote-history-persistence/quote-history-persistence-management.service';
import { HelperServicesModule } from '../helper-services/helper-services.module';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { FirmRatePersistenceManager } from './firm-rate-persistence/http/firm-rate-persistence-manager.service';
import { FirmRatePersistenceManagementService } from './firm-rate-persistence/firm-rate-persistence-management.service';

@NgModule({
  declarations: [],
  imports: [ CommonModule,
    JsonBindingModule,
    HttpClientModule,
    HelperServicesModule ],
  exports: [],
  providers: [QuotePersistenceManager,
    QuotePersistenceManagerManagement,
    StandardCostItemPersistenceManager,
    StandardCostItemPersistenceManagement,
    QuoteHistoryPersistenceManager,
    QuoteHistoryPersistenceManagement,
    FirmPersistenceManager,
    FirmPersistenceManagement,
    UnMarshallerService,
    FirmRatePersistenceManager,
    FirmRatePersistenceManagementService
  ],
})
export class LegalModule {}
