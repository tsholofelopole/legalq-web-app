import { Response } from 'src/app/models/response.model';
import { Firm } from 'src/app/models/firm.model';

export class FindFirmResponse extends Response {

  constructor() {
    super();
  }

  public firm: Firm;

}
