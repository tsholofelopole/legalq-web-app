import { Request } from 'src/app/models/request.model';
import { FirmIdentifierCriteria } from 'src/app/models/firm-identifier-criteria.model';

export class FindFirmRequest extends Request {

  constructor(){
    super();
  }

  public firmIdentifierCriteria: FirmIdentifierCriteria;

}
