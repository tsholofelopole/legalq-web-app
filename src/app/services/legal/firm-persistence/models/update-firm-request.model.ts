import { Request } from 'src/app/models/request.model';
import { Firm } from 'src/app/models/firm.model';

export class UpdateFirmRequest extends Request {
  constructor() {
    super();
  }

  public firm: Firm;
}
