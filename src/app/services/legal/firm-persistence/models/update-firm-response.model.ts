import { Response } from 'src/app/models/response.model';

export class UpdateFirmResponse extends Response {
  constructor() {
    super();
  }
}
