import { Injectable } from '@angular/core';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { FirmPersistenceManager } from './http/firm-persistence-manager.service';
import { BehaviorSubject } from 'rxjs';
import { FindFirmRequest } from './models/find-firm-request.model';
import { FirmIdentifierCriteria } from 'src/app/models/firm-identifier-criteria.model';
import { FindFirmResponse } from './models/find-firm-response.model';
import { Exception } from '../../helper-services/exception-handling/exception.model';
import { ExceptionParserService } from '../../helper-services/exception-handling/exception-parser.service';
import { UpdateFirmResponse } from './models/update-firm-response.model';
import { Firm } from 'src/app/models/firm.model';
import { UpdateFirmRequest } from './models/update-firm-request.model';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class FirmPersistenceManagement {
  constructor(private unMarshallerService: UnMarshallerService,
              private firmPersistenceManager: FirmPersistenceManager,
              private exceptionparserService: ExceptionParserService) {}

  // observables
  findFirmObservable = new BehaviorSubject<FindFirmResponse>(null);
  findFirmException = new BehaviorSubject<Exception>(null);

  updateFirmObservable = new BehaviorSubject<UpdateFirmResponse>(null);
  updateFirmException = new BehaviorSubject<Exception>(null);

  findFirm(firmIdentifier: string) {

    const findfirmRequest = new FindFirmRequest();
    const firmIdentifierCriteria = new FirmIdentifierCriteria();
    firmIdentifierCriteria.firmLegalReference = firmIdentifier;
    findfirmRequest.firmIdentifierCriteria = firmIdentifierCriteria;

    this.firmPersistenceManager.findFirm(findfirmRequest).subscribe(
      (response: HttpResponse<FindFirmResponse>): any => {

        console.log('$$$$$$$4', response);

        const exception = null;
        //  response.headers.get('X-Throwable-Type');

        if (exception) {
          this.findFirmException.next(this.exceptionparserService.parse(exception, ''));
        }  else {
          // const data = response.json();
          let findFirmResponse = new FindFirmResponse();
          console.log("\n\ Response from service: ", response);
          findFirmResponse = this.unMarshallerService.unMarshallFromJson(response, FindFirmResponse);
          // findFirmResponse = this.unMarshallerService.unMarshallFromJson(FindFirmResponse, data);

          console.log('\n\n********Find Firm Response: ', findFirmResponse);

          this.findFirmObservable.next(findFirmResponse);
        }

    });

  }

  updateFirm(firm: Firm) {

    const updateFirmRequest = new UpdateFirmRequest();
    updateFirmRequest.firm = firm;
    this.firmPersistenceManager.updateFirm(updateFirmRequest).subscribe(
      (response: HttpResponse<UpdateFirmResponse>): any => {
        // const exception = null;
        console.log("\n\ Response from service: ", response);
        const exception = null;
        //  response.headers.get('X-Throwable-Type');

        if (exception) {
          this.findFirmException.next(this.exceptionparserService.parse(exception, ''));
        }  else {
          // const data = response.json();
          let updateFirmResponse = new UpdateFirmResponse();
          updateFirmResponse = this.unMarshallerService.unMarshallFromJson(response, UpdateFirmResponse);
          // updateFirmResponse = this.unMarshallerService.unMarshallFromJson(UpdateFirmResponse, data);

          console.log("\n\nUpdate firm reponse: ",updateFirmResponse );
          this.updateFirmObservable.next(updateFirmResponse);
        }

      });
  }

  handleException(exception: Exception) {
    this.findFirmException.next(exception);
  }

}
