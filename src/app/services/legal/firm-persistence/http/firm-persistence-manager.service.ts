import { Injectable } from '@angular/core';
import { MarshallerService } from 'src/app/json-binding/marshaller.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FindFirmRequest } from '../models/find-firm-request.model';
import { UpdateFirmRequest } from '../models/update-firm-request.model';

@Injectable()
export class FirmPersistenceManager {

  constructor(private http: HttpClient,
              private marshallerService: MarshallerService)  {}

    firmUri = 'http://' + environment.host + ':' + environment.port + '/legalq/api/org/legal/firmpersistencemanager';

    private headers = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });

    findFirm(findFirmRequest: FindFirmRequest) {

      const findFirmUri = this.firmUri + '/findfirm';
      findFirmRequest = this.marshallerService.marshallObjectToJSON(findFirmRequest);
      console.log('\n\nFind firm req: ', findFirmRequest);

      return this.http.post(findFirmUri, findFirmRequest, {headers : this.headers});
    }

    updateFirm(updateFirmRequest: UpdateFirmRequest) {

      const updateFirmUri = this.firmUri + '/updatefirm';
      updateFirmRequest = this.marshallerService.marshallObjectToJSON(updateFirmRequest);
      console.log('\n\n&&&&&&&&&&&&&&&&&&&& Update firm Req: ', updateFirmRequest);

      return this.http.post(updateFirmUri, updateFirmRequest, {headers : this.headers});
    }

}
