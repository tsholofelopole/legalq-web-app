import { Injectable } from '@angular/core';


import { InsertQuoteResponse } from './models/insert-quote-reponse.model';

import { FindQuoteResponse } from './models/find-quote-response.model';
import { BehaviorSubject } from 'rxjs';
import { QuotePersistenceManager } from './http/quote-persistence-manager.service';
import { InsertQuoteRequest } from './models/insert-quote-request.model';
import { Exception } from '../../helper-services/exception-handling/exception.model';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { ExceptionParserService } from '../../helper-services/exception-handling/exception-parser.service';
import { FindQuoteRequest } from './models/find-quote-request.model';

@Injectable()
export class QuotePersistenceManagerManagement {

  constructor(private quotePersistenceManager: QuotePersistenceManager,
              private unMarshallerService: UnMarshallerService,
              private exceptionparserService: ExceptionParserService, ) {}

  insertQuoteResponseObservable = new BehaviorSubject<InsertQuoteResponse>(null);
  findQuoteResponseObservable = new BehaviorSubject<FindQuoteResponse>(null);
  insertQuoteExceptionObservable = new BehaviorSubject<Exception>(null);
  findQuoteExceptionObservable = new BehaviorSubject<Exception>(null);

  insertQuote(insertQuoteRequest: InsertQuoteRequest) {

    this.quotePersistenceManager.insertQuote(insertQuoteRequest).subscribe(
      (response: Response): any => {
        const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.insertQuoteExceptionObservable.next(this.exceptionparserService.parse(exception, ''));
        } else {
          const data = response.json();
          let insertQuoteResponse = new InsertQuoteResponse();
          insertQuoteResponse = this.unMarshallerService.unMarshallFromJson(data, InsertQuoteResponse);
          console.log('InsertQuoteHistory Response: ', insertQuoteResponse);
          this.insertQuoteResponseObservable.next(insertQuoteResponse);

        }
      }
    );
  }

  findQuote(findQuoteRequest: FindQuoteRequest) {

    this.quotePersistenceManager.findOneQuote(findQuoteRequest).subscribe(
      (response: Response): any => {
        const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.findQuoteExceptionObservable.next(this.exceptionparserService.parse(exception, ''));
        } else {
          const data = response.json();
          let findQuoteResponse = new FindQuoteResponse();
          findQuoteResponse = this.unMarshallerService.unMarshallFromJson(data, FindQuoteResponse);
          console.log('InsertQuoteHistory Response: ', findQuoteResponse);
          this.insertQuoteResponseObservable.next(findQuoteResponse);

        }
      }
    );
  }

}
