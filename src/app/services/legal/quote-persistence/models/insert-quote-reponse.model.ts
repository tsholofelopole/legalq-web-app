import { Response } from 'src/app/models/response.model';

export class InsertQuoteResponse extends Response {
  constructor() {
    super();
  }
}
