import { Request } from 'src/app/models/request.model';
import { Criteria } from 'src/app/models/criteria.model';

export class FindQuoteRequest extends Request {
  constructor() {
    super();
  }

  public criteria: Criteria;
}
