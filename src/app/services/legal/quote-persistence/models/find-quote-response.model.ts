import { Response } from 'src/app/models/response.model';
import { Quote } from 'src/app/models/quote.model';

export class FindQuoteResponse extends Response {
  constructor() {
    super();
  }

  public quotes: Quote;
}
