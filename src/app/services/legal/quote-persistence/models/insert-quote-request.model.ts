import { Request } from 'src/app/models/request.model';
import { Quote } from 'src/app/models/quote.model';

export class InsertQuoteRequest extends Request {
  constructor() {
    super();
  }

  public quote: Quote;
}
