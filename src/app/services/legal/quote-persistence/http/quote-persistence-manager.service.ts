import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FindQuoteRequest } from '../models/find-quote-request.model';
import { MarshallerService } from 'src/app/json-binding/marshaller.service';
import { InsertQuoteRequest } from '../models/insert-quote-request.model';

@Injectable()
export class QuotePersistenceManager {

  constructor(private http: HttpClient,
              private marshallerService: MarshallerService)  {}

  quoteUri = 'http://' + environment.host + ':' + environment.port + '/legalq/api/org/legal/quotepersistencemanager';

  private headers = new HttpHeaders ({
    'Content-Type': 'application/json'
  });

  findOneQuote(findQuoteRequest: FindQuoteRequest) {

    const findQuoteUri = this.quoteUri + '/findquote';
    findQuoteRequest = this.marshallerService.marshallObjectToJSON(findQuoteRequest);
    console.log('\n\nReq Object: ', findQuoteRequest);
    return this.http.post(findQuoteUri, findQuoteRequest,  {headers : this.headers});
  }

  insertQuote(insertQuoteRequest: InsertQuoteRequest) {

    const insertQuoteUri = this.quoteUri + '/insertquote';

    insertQuoteRequest = this.marshallerService.marshallObjectToJSON(insertQuoteRequest);
    console.log("Req Object: ", insertQuoteRequest);

    return this.http.post(insertQuoteUri, insertQuoteRequest,  {headers : this.headers});
  }
}
