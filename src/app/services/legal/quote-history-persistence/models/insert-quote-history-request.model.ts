import { Request } from 'src/app/models/request.model';
import { QuoteHistory } from 'src/app/models/quote-history.model';

export class InsertQuoteHistoryRequest extends Request {
  constructor() {
    super();
  }

  public quoteHistory: QuoteHistory;
}
