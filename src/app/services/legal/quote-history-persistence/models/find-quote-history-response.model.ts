import { Response } from 'src/app/models/response.model';
import { QuoteHistory } from 'src/app/models/quote-history.model';

export class FindQuoteHistoryResponse extends Response {

  constructor() {
    super();
  }

  public quoteHistories: QuoteHistory[] = [];
}
