import { Response } from 'src/app/models/response.model';

export class InsertQuoteHistoryResponse extends Response {
  constructor() {
    super();
  }
}
