import { Request } from 'src/app/models/request.model';
import { Criteria } from 'src/app/models/criteria.model';

export class FindQuoteHistoryRequest extends Request {
  constructor() {
    super();
  }

  public criteria: Criteria;
}
