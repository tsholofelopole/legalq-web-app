import { Injectable } from '@angular/core';
import { MarshallerService } from 'src/app/json-binding/marshaller.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FindQuoteHistoryRequest } from '../models/find-quote-history-request.model';
import { InsertQuoteHistoryRequest } from '../models/insert-quote-history-request.model';

@Injectable()
export class QuoteHistoryPersistenceManager {

 constructor(private http: HttpClient,
            private marshallerService: MarshallerService)  {}

    quotehistoryUri = 'http://' + environment.host + ':' + environment.port + '/legalq/api/org/legal/quotehistorypersistencemanager';

    private headers = new HttpHeaders ({
      'Content-Type': 'application/json'
    });

  findQuoteHistorySummaries(findQuoteHistoryRequest: FindQuoteHistoryRequest) {

    const findQuoteHisoryUri = this.quotehistoryUri + '/findquotehistorysummaries';
    findQuoteHistoryRequest = this.marshallerService.marshallObjectToJSON(findQuoteHistoryRequest);
    console.log('Find Quote History req: ', findQuoteHistoryRequest);
    return this.http.post(findQuoteHisoryUri, findQuoteHistoryRequest, {headers : this.headers});
  }

  insertQuoteHistorySummary(insertQuoteHistoryRequest: InsertQuoteHistoryRequest) {

    const insertQuoteHistoryUri = this.quotehistoryUri + '/insertquotehistorysummary';
    insertQuoteHistoryRequest = this.marshallerService.marshallObjectToJSON(insertQuoteHistoryRequest);
    console.log('Quote history req:', insertQuoteHistoryRequest);

    return this.http.post(insertQuoteHistoryUri, insertQuoteHistoryRequest, {headers : this.headers});
  }
}
