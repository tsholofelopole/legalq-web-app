import { Injectable } from '@angular/core';
import { FindQuoteHistoryRequest } from './models/find-quote-history-request.model';
import { InsertQuoteHistoryRequest } from './models/insert-quote-history-request.model';
import { UnMarshallerService } from 'src/app/json-binding/unmarshaller.service';
import { ExceptionParserService } from '../../helper-services/exception-handling/exception-parser.service';
import { QuoteHistoryPersistenceManager } from './http/quote-history-persistence-manager.service';
import { BehaviorSubject } from 'rxjs';
import { FindQuoteHistoryResponse } from './models/find-quote-history-response.model';
import { InsertQuoteHistoryResponse } from './models/insert-quote-history-response.model';
import { Exception } from '../../helper-services/exception-handling/exception.model';

@Injectable()
export class QuoteHistoryPersistenceManagement {

  constructor(private unMarshallerService: UnMarshallerService,
              private exceptionparserService: ExceptionParserService,
              private quoteHistoryPersistenceManager: QuoteHistoryPersistenceManager) {}

  insertQuoteHistoryResponseObservable = new BehaviorSubject<InsertQuoteHistoryResponse> (null);
  findQuoteHistoryResponseObservable = new BehaviorSubject<FindQuoteHistoryResponse>(null);
  exception = new BehaviorSubject<Exception>(null);

  findQuoteHistory(findQuoteHistoryRequest: FindQuoteHistoryRequest) {

    this.quoteHistoryPersistenceManager.findQuoteHistorySummaries(findQuoteHistoryRequest).subscribe(
      (response: Response): any => {

        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          console.log("QuoteResponse: ", response);
          // const data = response.json();
          let findQuoteHistoryResponse = new FindQuoteHistoryResponse();
          findQuoteHistoryResponse = this.unMarshallerService.unMarshallFromJson(response, FindQuoteHistoryResponse);
          // findQuoteHistoryResponse = this.unMarshallerService.unMarshallFromJson(data, FindQuoteHistoryResponse);
          console.log('\n\n', findQuoteHistoryResponse);
          this.findQuoteHistoryResponseObservable.next(findQuoteHistoryResponse);
        }

      });
  }

  insertQuoteHistory(insertQuoteHistoryRequest: InsertQuoteHistoryRequest) {
    this.quoteHistoryPersistenceManager.insertQuoteHistorySummary(insertQuoteHistoryRequest).subscribe(
      (response: Response): any => {
        const exception = null;
        // const exception = response.headers.get('X-Throwable-Type');

        if (exception) {
            this.exception.next(this.exceptionparserService.parse(exception, ''));
        } else {
          // const data = response.json();
          let insertQuoteHistoryResponse = new InsertQuoteHistoryResponse();
          insertQuoteHistoryResponse = this.unMarshallerService.unMarshallFromJson(response, InsertQuoteHistoryResponse);
          // insertQuoteHistoryResponse = this.unMarshallerService.unMarshallFromJson(data, InsertQuoteHistoryResponse);
          this.insertQuoteHistoryResponseObservable.next(insertQuoteHistoryResponse);
        }

      });
  }
}
