import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './view-components/login/login.module';
import { HeaderModule } from './view-components/header/header.module';
import { FooterModule } from './view-components/footer/footer.module';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ForgotPasswordModule } from './view-components/forgot-password/forgot-password.module';
import { HomeModule } from './view-components/home/home.module';
import { QuoteModule } from './view-components/quote/quote.module';
import { AdminModule } from './view-components/admin/admin.module';
import { HistoryModule } from './view-components/history/history.module';
import { SessionmanagerModule } from './view-components/sessionmanager/sessionmanager.module';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HistorycontentModule } from './view-components/historycontent/historycontent.module';
import { RequestpasswordchangeModule } from './view-components/requestpasswordchange/requestpasswordchange.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomloaderModule } from './view-components/customloader/customloader.module';
import { CustomLoaderService } from './services/view-services/cuctom-loader.service';
import { MattertypeModule } from './view-components/mattertype/mattertype.module';
import { QuotetypeModule } from './view-components/quotetype/quotetype.module';
import { AuthenticatorModule } from './services/legal/authenticator/authenticator.module';
import { JsonBindingModule } from './json-binding/json-binding.module';
import { LegalModule } from './services/legal/legal.module';
import { HelperServicesModule } from './services/helper-services/helper-services.module';
import { SessionManagerService } from './services/view-services/session-manager.service';
import { QuoteDocumentModule } from './view-components/quote-document/quote-document.module';
import { DisplayQuoteManager } from './services/view-services/display-quote-manager.service';
import { FirmTemplateModule } from './view-components/firm-template/firm-template.module';
import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
  BrowserModule,
    AppRoutingModule,
    CommonModule,
    LoginModule,
    HeaderModule,
    FooterModule,
    BsDropdownModule.forRoot(),
    ForgotPasswordModule,
    HomeModule,
    QuoteModule,
    AdminModule,
    HistoryModule,
    SessionmanagerModule,
    NgIdleKeepaliveModule.forRoot(),
    HttpClientModule,
    HistorycontentModule,
    RequestpasswordchangeModule,
    AccordionModule.forRoot(),
    CustomloaderModule,
    MattertypeModule,
    QuotetypeModule,
    AuthenticatorModule,
    JsonBindingModule,
    LegalModule,
    HelperServicesModule,
    QuoteDocumentModule,
    FirmTemplateModule

  ],
  providers: [HttpClient,
    CustomLoaderService,
    SessionManagerService,
    DisplayQuoteManager,
    {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
